if exists("did_load_filetypes")
  finish
endif
augroup filetypedetect
  au! BufRead,BufNewFile *.maxima		setfiletype maxima
  au! BufRead,BufNewFile *.max		setfiletype maxima
  au! BufRead,BufNewFile *.mxm		setfiletype maxima
augroup END

