if exists("b:did_ftplugin")
  finish
endif

compiler saxonb-xquery

if !exists(":RunWith")
  setlocal makeprg=saxonb-xquery\ %
  map <buffer> <F9> :make<CR>
  imap <buffer> <F9> <Esc>:make<CR>
else
  map <buffer> <F9> :RunWith saxonb-xquery<CR>
  vmap <buffer> <F9> :RunSelection saxonb-xquery<CR>
  imap <buffer> <F9> <Esc>:RunWith saxonb-xquery<CR>
endif

let b:did_ftplugin=1
