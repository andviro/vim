if exists("did_load_filetypes")
  finish
endif
augroup filetypedetect
  au! BufRead,BufNewFile *.spice		setfiletype spice
  au! BufRead,BufNewFile *.spi		setfiletype spice
  au! BufRead,BufNewFile *.sp		setfiletype spice
  au! BufRead,BufNewFile *.sp2		setfiletype spice
  au! BufRead,BufNewFile *.sp3		setfiletype spice
  au! BufRead,BufNewFile *.PRM		setfiletype spice
  au! BufRead,BufNewFile *.cir		setfiletype spice
augroup END

