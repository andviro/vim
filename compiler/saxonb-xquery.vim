" Vim Compiler File
" Compiler:	saxonb-xquery

if exists("current_compiler")
    finish
endif
let current_compiler = "saxonb-xquery"

if exists(":CompilerSet") != 2		" older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

let s:cpo_save = &cpo
set cpo&vim

CompilerSet makeprg=saxonb-xquery

CompilerSet errorformat=%EError\ on\ line\ %l\ column\ %c\ of\ %f:,%C%m,%Z,%EError\ on\ line\ %l\ of\ %f:,%C%m,%Z

let &cpo = s:cpo_save
unlet s:cpo_save
