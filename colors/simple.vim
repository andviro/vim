hi User1 cterm=none ctermfg=blue ctermbg=white
hi User2 cterm=bold ctermfg=red ctermbg=white
hi User3 cterm=none ctermfg=green ctermbg=white
hi Statusline cterm=none ctermbg=black ctermbg=white
hi StatuslineNC cterm=none ctermbg=black ctermbg=white
hi Visual ctermbg=39
hi MatchParen cterm=bold ctermbg=none
hi Pmenu ctermbg=7 guibg=Grey
hi PmenuSel ctermbg=41 guibg=LightMagenta

