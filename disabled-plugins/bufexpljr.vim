" vim: ts=2 sw=2 et fdm=marker
" Plugin: BufExplorerJr --- minimalistic buffer explorer in SideBar
" Version: 0.2
" $Id: bufexpljr.vim,v 1.15 2003/10/18 11:14:09 andrew Exp $
"
" Author: Andrew Rodionoff <arnost AT mail DOT ru>
"
" Description: manages buffer list in SideBar. Credits to guys who came up
" with the idea, I don't know who was the first. Personally I liked
" BufExplorer by Jeff Lanzarotta and MiniBufExplorer by Bindu Wavell.
"
" Usage: :SideBarSwallow BufExplorerJr
" press <CR> on buffer to visit
"
" Tip: Try including following autocommand in your setup:
" au BufAdd * SideBarAddCmd BufExplorerJr
"
" New in version 0.2:
" - Buffer numbers are now in second column, so there's more info in withdrawn
"   state.
" - Buffer additions and deletions are now handled more reliably.
"
" Autocommands
let s:safe = 1
let s:dirty = 0
let s:active_buffer = -1

augroup BufExplorerJr "{{{
  au!
  au BufEnter,BufAdd * call <SID>Refresh(-1)
  au BufDelete * call <SID>Refresh(expand('<abuf>') + 0)
augroup END "}}}

" Internals
fun! s:Create() "{{{
  if bufnr('BufExplorerJr') != -1
    buffer BufExplorerJr
    return
  endif
  let s:safe = 0
  edit BufExplorerJr
  setlocal noswf nobuflisted noma nomod ro
  nmap <silent> <buffer> <CR> :call <SID>Enter()<CR>
  syn clear
  setlocal ts=100
  let s:safe = 1
endfun "}}}

fun! s:Refresh(deleteme) "{{{
  if !s:safe
    return
  endif
  let s:safe = 0
  let l:w = bufwinnr('BufExplorerJr')
  let l:bnum = bufnr('%')
  if l:w != -1 
    if l:w != winnr()
      exec l:w . 'wincmd w'
      call s:ReFill(l:bnum, a:deleteme)
      wincmd p
    elseif s:dirty
      call s:ReFill(-1, -1)
    endif
  else
    let s:dirty = 1
    let s:active_buffer = l:bnum
  endif
  let s:safe = 1
endfun "}}}

fun! s:ReFill(bnum, deleteme) "{{{
  setlocal ma noro
  silent %d _
  let l:n = 1
  let l:last = bufnr('$')
  while l:n <= l:last
    if bufexists(l:n) && buflisted(l:n) && a:deleteme != l:n
      let l:bn = bufname(l:n)
      let l:bn_tail = fnamemodify(l:bn, ':t')
      if l:bn_tail == ''
        let l:bn_tail = '[No File]'
      endif
      let l:a = l:bn_tail .
            \ ' (' . fnamemodify(l:bn, ':p:~:h') .')' . "\t" . l:n
      silent put =l:a
    endif
    let l:n = l:n + 1
  endwhile
  silent 0d _
  setlocal nomod ro noma nobuflisted
  syn clear Search
  if a:bnum != -1
    let s:active_buffer = a:bnum
  endif
  if s:active_buffer != -1
    exec 'syn match Search +^.*\t' . s:active_buffer . '$+'
    silent! exec '0;/\t' . s:active_buffer . '$/;'
  endif
  let s:dirty = 0
endfun "}}}

fun! s:Enter() "{{{
  let l:cl = getline('.')
  let s:safe=0
  let l:bn = strpart(l:cl, stridx(l:cl, "\t") + 1) + 0
  call s:ReFill(l:bn, -1)
  normal 0
  wincmd p
  exec 'buffer ' . l:bn
  let s:safe=1
endfun "}}}

command! BufExplorerJr call <SID>Create()

