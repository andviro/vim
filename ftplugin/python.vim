setlocal nowrap
setlocal nonumber
setlocal foldmethod=indent
setlocal errorformat=\ \ File\ \"%f\"\\,\ line\ %l\\,\ %m

"compiler pyunit
"set efm=%C\ %.%#,%A\ \ File\ \"%f\"\\,\ line\ %l%.%#,%Z%[%^\ ]%\\@=%m
"aug Python
    "au!
    "au BufEnter *.py setlocal errorformat=\ \ File\ \"%f\"\\,\ line\ %l\\,\ %m
"aug end

if !exists('*s:getPython')
    fun s:getPython()
        if exists('b:AutoVirtualEnv')
            if executable(b:AutoVirtualEnv . '/bin/python')
                return b:AutoVirtualEnv . '/bin/python'
            endif
            if executable(b:AutoVirtualEnv . '\\Scripts\\python.exe')
                return b:AutoVirtualEnv . '\\Scripts\\python.exe'
            endif
        return 'python'
    endfun

    fun s:getNosetests()
        if exists('b:AutoVirtualEnv')
            if executable(b:AutoVirtualEnv . '/bin/nosetests')
                return b:AutoVirtualEnv . '/bin/nosetests'
            endif
            if executable(b:AutoVirtualEnv . '\\Scripts\\nosetests.exe')
                return b:AutoVirtualEnv . '\\Scripts\\nosetests.exe'
            endif
        return 'nosetests'
    endfun
endif

if !hasmapto('<Plug>RunPython')
    nmap <buffer> <Leader>r <Plug>RunPython
    imap <buffer> <F9> <Plug>RunPython
    vmap <buffer> <Leader>r <Plug>RunPython
endif

"if !hasmapto('<Plug>RunNosetests')
    "map <buffer> <unique> <Leader>t <Plug>RunNosetests
"endif
if !exists(":RunWith")
  setlocal makeprg=python\ %
  nmap <buffer>  <Plug>RunPython :make<CR>
  imap <buffer>  <Plug>RunPython <ESC>:make<CR>
else
  nmap <buffer>  <Plug>RunPython :exec ':RunWith ' . <SID>getPython()<CR>
  vmap <buffer>  <Plug>RunPython :exec ':RunSelection ' . <SID>getPython()<CR>
  imap <buffer>  <Plug>RunPython <Esc>:exec ':RunWith ' . <SID>getPython()<CR>
  map <buffer> <Plug>RunNosetests :exec ':RunCmd ' . <SID>getNosetests() + ' -v'<CR>
endif

if exists(":RopeGotoDefinition")
    nnoremap <buffer> <CR> :RopeGotoDefinition<CR>
endif
if exists(":RopeRename")
    nnoremap <buffer> ;R :RopeRename<CR>
endif
