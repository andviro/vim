" vim: ts=2 sw=2 et fdm=marker cms=\ \"%s
" Plugin: ProjectJr --- simplistic rewrite of Aric Blumer's Project plugin
" Version: 0.3.1
" $Id: projectjr.vim,v 1.56 2003/10/11 15:54:49 andrew Exp $
"
" Author: Andrew Rodionoff <arnost AT mail DOT ru>
"
" Description:
" This mini-plugin provides jump-list to often-used files. There can be many
" such lists saved in files with .prj extension, each one may be loaded
" separately with command :ProjectJr <filename>. Here's example of such
" jump-list file, I hope it's self-explanatory
"
"~/
"  .Xdefaults
"  .Xclients-default
"  .ctags
"
"~/vim/
"  _vimrc
"  plugin/{\.vim$}
"    SideBar.vim
"    projectjr.vim
"    taglistjr.vim
"    rcs.vim
"    latex-make.vim
"    latexmaps.vim
"    xmmsctrl.vim
"    vimexec.vim
"  ftplugin/
"    tex.vim
"  latex/
"    auctex.vim
"  indent/
"    tex.vim
"
" Lines with no indent provide base directory to construct full path to file.
" Sub-projects are constructed using indentation. Lines with the same indent
" level are considered separate entries. You can press <Enter> on any line to
" load corresponding file into editor. Blank lines and lines, starting with
" '"' are ignored.
"
" New in v0.3.1:
" - better escaping of filenames
" - exported function ProjectJr_PathAtLine
" - buffer variable b:IsProjectJr_buffer
" New in v0.3:
" - Each directory entry may have an optional filter modifier in curly
"   braces. E.g. to list only python files use:
"   python_src/{\.py$}
"     a.py
"     b.py
"     c.py
"   Note: subdirectories are not subject to filtering. Also note that entry
"   filters are 'inherited' by all its subdirs.
"
" - Refresh command without arguments defaults to current entry filter
"   modifier.
"
" - Now that entry filters and collapse/expand feature are available, folding
"   is switched off by default. Use zi to toggle it on or set
"   g:ProjectJr_start_folded = 1 to enable old behaviour.
"
" New in v0.2:
" - Pressing <Enter> on directory name opens or collapses its contents
"   listing. Note that 'wildignore' and 'suffixes' options are in effect here. 
"
" - New command :Refresh <regexp-pattern>
"   Use it in project buffer to re-scan entry at current line and leave only
"   sub-entries that match <regexp-pattern>

" - You can control placement of subdirectories in newly-opened listing using
"   variable g:ProjectJr_dirs_first. See also g:ProjectJr_show_dotfiles and
"   g:ProjectJr_dotfiles_first below.
"
if !exists('g:ProjectJr_dirs_first')
  let g:ProjectJr_dirs_first = 0 " looks better to me
endif

" You can control whether to show or hide .* filenames using variable
" g:ProjectJr_show_dotfiles
"
if !exists('g:ProjectJr_show_dotfiles')
  let g:ProjectJr_show_dotfiles = 1
endif

" Will we put dotfiles before or after main file listing?
if !exists('g:ProjectJr_dotfiles_first')
  let g:ProjectJr_dotfiles_first = 1
endif

" Fold entries on startup?
if !exists('g:ProjectJr_start_folded')
  let g:ProjectJr_start_folded = 0
endif

" Internals
fun! PRJ_FoldText() "{{{
  let l:l = v:foldstart
  let l:idt = indent(l:l)
  let l:res = ''
  while l:l <= v:foldend
    if l:idt == indent(l:l)
      let l:res = l:res . "[" . s:Strip(getline(l:l)) . "]"
    endif
    let l:l = l:l + 1
  endwhile
  return substitute(getline(v:foldstart), '^\(\s*\).*', '\1', '') . l:res
endfun "}}}

fun! s:Project(file) "{{{
  if bufexists(a:file)
    exec 'buffer ' . a:file
  else
    exec 'edit ' . a:file
    setlocal sw=2 ts=2 et nobl noswf ar
    setlocal fdm=expr fde=indent(v:lnum) fdt=PRJ_FoldText()
    if !g:ProjectJr_start_folded
      set nofoldenable
    endif
    syn match PrjComment '^\s*".*'
    syn match PrjSubdir '.*/\s*\({.*}\)\?$' contains=PrjOpts
    syn match PrjOpts '{.*}$' contained
    hi link PrjSubdir Statement
    hi link PrjOpts String
    hi link PrjComment Comment
    nmap <silent> <buffer> <CR> :call <SID>Enter()<CR>
    command! -buffer -nargs=? Refresh call <SID>RefreshEntry(<f-args>)
    let b:IsProjectJr_buffer = 1
  endif
endfun "}}}

fun! s:Strip(str) "{{{
  let l:left = substitute(a:str, '^\s*\(.*\)', '\1', '')
  let l:right = substitute(l:left, '\(.\{-}\)\s*$', '\1', '')
  return l:right
endfun "}}}

fun! s:RemoveOpts(fname) "{{{
  return substitute(a:fname, '\s*{.*}$', '', '')
endfun "}}}

fun! s:GetOpts(line) "{{{
  let l:line = a:line
  let l:idt = indent(l:line)
  let l:str = s:Strip(getline(l:line))

  while l:line > 1 && l:idt > 0 && l:str !~ '{.*}$'
    let l:line = l:line - 1
    if l:str !~ '^\s*\($\|"\)' && indent(l:line) < l:idt
      let l:str = s:Strip(getline(l:line))
    endif
  endwhile

  if l:str =~ '{.*}$'
    return substitute(l:str, '.*{\(.*\)}$', '\1', '')
  else
    return ''
  endif
endfun "}}}

fun! s:PathAtLine(line) "{{{
  let l:line = a:line
  let l:idt = indent(l:line)
  let l:fname = s:RemoveOpts(s:Strip(getline(l:line)))
  if l:fname =~ '^\s*\($\|"\)'
    return ''
  endif

  while l:line > 1 && l:idt > 0
    let l:line = l:line - 1
    let l:str = s:Strip(getline(l:line))
    if l:str !~ '^\s*\($\|"\)' && indent(l:line) < l:idt
      let l:idt = indent(l:line)
      let l:fname = s:RemoveOpts(l:str) . l:fname
    endif
  endwhile
  return l:fname
endfun "}}}

fun! s:CloseEntry(line) "{{{
  let l:n = s:EntrySize(a:line)
  if l:n
    silent exec (a:line + 1) . ',' . (a:line + l:n) . 'delete _'
    exec a:line . ';'
  endif
endfun "}}}

fun! s:OpenEntry(line, mask) "{{{
  let l:basepath = s:PathAtLine(a:line)
  let l:entries = glob(fnameescape(l:basepath) . '*')
  if g:ProjectJr_show_dotfiles
    if g:ProjectJr_dotfiles_first
      let l:entries = glob(l:basepath . '.*') . "\n" . l:entries
    else
      let l:entries = l:entries . "\n" . glob(l:basepath . '.*')
    endif
  endif
  let l:line = a:line
  let l:idt = indent(a:line)
  let l:l1 = a:line
  let l:n = 0
  while l:entries != ''
    let l:pos = stridx(l:entries, "\n")
    if l:pos == -1
      let l:pos = strlen(l:entries)
    endif
    let l:entry = strpart(l:entries, 0, l:pos)
    let l:entries = strpart(l:entries, l:pos + 1)
    let l:tail = fnamemodify(l:entry, ':t')
    if l:tail =~ '^\.\.\?$'
      continue
    endif
    if isdirectory(l:entry)
      let l:tail = l:tail . '/'
      silent! exec l:l1 . 'put =l:tail'
      let l:l1 = l:l1 + 1
      if g:ProjectJr_dirs_first
        let l:line = l:line + 1
      endif
    else
      if l:tail !~ a:mask
        continue
      endif
      silent! exec l:line . 'put =l:tail'
      let l:line = l:line + 1
      if !g:ProjectJr_dirs_first
        let l:l1 = l:l1 + 1
      endif
    endif
    let l:n = l:n + 1
  endwhile
  if l:n
    exec (a:line + 1) . ',' . (a:line + l:n) . 'left ' . (l:idt + &sw)
    exec a:line . ';'
  endif
endfun "}}}

fun! s:EntrySize(line) "{{{
  let l:n = 1
  let l:idt = indent(a:line)
  while (indent(a:line + l:n) > l:idt
        \ || getline(a:line + l:n) =~ '^\s*\($\|"\)')  && (a:line + l:n) <= line('$')
    let l:n = l:n + 1
  endwhile
  let l:n = l:n - 1
  if l:n && getline(a:line + l:n) =~ '^\s*\($\|"\)'
    let l:n = l:n - 1
  endif
  return l:n
endfun "}}}

fun! s:RefreshEntry(...) "{{{
  let l:line = line('.')
  while (getline(l:line) =~ '^\s*\($\|"\)'
        \ || !isdirectory(fnamemodify(s:PathAtLine(l:line), ':p'))) 
        \ && l:line > 0
    let l:line = l:line - 1
  endwhile
  if l:line == 0
    return
  endif
  call s:CloseEntry(l:line)
  if a:0
    let l:mask = a:1
  else
    let l:mask = s:GetOpts(l:line)
    if l:mask == ''
      let l:mask = '.*'
    endif
  endif
  call s:OpenEntry(l:line, l:mask)
endfun "}}}

fun! s:Enter() "{{{
  let l:line = line('.')
  let l:fname = s:PathAtLine(l:line)
  let l:mask = s:GetOpts(l:line)
  if l:mask == ''
    let l:mask = '.*'
  endif
  if l:fname == ''
    return
  elseif foldclosed(l:line) != -1
    normal zO
  elseif isdirectory(fnamemodify(l:fname, ':p'))
    if s:EntrySize(l:line) > 0
      call s:CloseEntry(l:line)
    else
      call s:OpenEntry(l:line, l:mask)
    endif
  else
    exec 'normal ' . indent(l:line) . 'zl'
    wincmd p
    exec 'edit ' . fnameescape(l:fname)
  endif
endfun "}}}

" Exported commands and functions {{{
command! -nargs=1 -complete=file ProjectJr call <SID>Project(<f-args>)
fun! ProjectJr_PathAtLine(line)
  return s:PathAtLine(a:line)
endfun
" }}}

