if exists("b:did_ftplugin")
  finish
endif

compiler spice

map <buffer> <F8> :GSCH<CR>
imap <buffer> <F8> <Esc>:GSCH<CR>
map <buffer> <F9> :make<CR>
imap <buffer> <F9> <Esc>:make<CR>

let b:did_ftplugin=1
