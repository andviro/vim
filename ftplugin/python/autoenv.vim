" Check python support
if !has('python')
    echo "Error: virtualenv support requires vim compiled with +python."
    finish
endif

if !exists('g:AutoVirtualEnvDirectory')
    let g:AutoVirtualEnvDirectory = expand('<sfile>:p:h')
python << EOF
import sys
import os
import vim
sys.path.insert(0, vim.eval("g:AutoVirtualEnvDirectory"))
from autoenv import find_env, setup_path
EOF
endif

function! autoenv#init(fn)
    py path = vim.eval('a:fn')
    py vim.command('let b:AutoVirtualEnv = "{0}"'.format((find_env(path) or '').replace('\\', '\\\\')))
    call autoenv#activate()
endfun

function! autoenv#activate()
py << EOF
ve_dir = vim.eval('b:AutoVirtualEnv')
if setup_path(ve_dir):
    activate_this = os.path.join(os.path.join(ve_dir, 'bin'), 'activate_this.py')
    # Fix for windows
    if not os.path.exists(activate_this):
        activate_this = os.path.join(os.path.join(ve_dir, 'Scripts'), 'activate_this.py')
    execfile(activate_this, dict(__file__=activate_this))
EOF
endfun

if !exists('b:AutoVirtualEnv')
    call autoenv#init(expand('%:p:h'))
    au BufEnter <buffer> call autoenv#activate()
endif
