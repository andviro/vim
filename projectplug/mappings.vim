" vim: ft=vim
let s:sid = substitute(maparg('<Return>', 'n'), '.*\(<SNR>.\{-}\)_.*', '\1', '')
function! s:LaunchOrWhat()
    let fname=Project_GetFname(line('.'))
    if fname =~ '\.\(jpe?g\|gif\|png\)$'
        exec 'silent! !qiv '.fname.' >/dev/null 2>&1 &'
        redraw!
    elseif fname =~ '\.obj$'
        exec 'silent! !LANG=ru_RU.KOI8-R && tgif '.fname.' >/dev/null 2>&1 &'
        redraw!
    elseif fname =~ '\.fig$'
        exec 'silent! !LANG=ru_RU.KOI8-R && xfig '.fname.' >/dev/null 2>&1 &'
        redraw!
    else
"        let xxx = foldclosed('.')
        call {s:sid}_DoFoldOrOpenEntry('', 'e')
"        if xxx == -1 && g:proj_window_is_narrow == 1
"            call {s:sid}_DoToggleProject()
"        endif
    endif
endfunction
"if g:proj_window_is_narrow == 1
"    nmap <buffer> <Tab> <Plug>ToggleProject
"else
"endif
nnoremap <buffer> <silent> <Return>   \|:call <SID>LaunchOrWhat()<CR>
nnoremap <silent> <buffer> <Tab> :wincmd p<CR>

fun! s:ShrinkProject()
    if exists('g:proj_running') && bufwinnr(g:proj_running) != -1
        if bufwinnr(g:proj_running) != winnr()
            let l:back = GotoWindow(bufwinnr(g:proj_running))
            exe (&columns - &tw - 1) . 'wincmd |'
            call GotoWindow(l:back)
        endif
    endif
endfun

fun! s:EnsureProject()
    if exists('g:proj_running') && bufwinnr(g:proj_running) != -1
        if bufwinnr(g:proj_running) == winnr()
            wincmd w
        endif
    endif
endfun

let bufn = bufname('%')
augroup ProjectUtils
    au!
    au BufLeave * call <SID>EnsureProject()
    au BufEnter * call <SID>ShrinkProject()
augroup END
