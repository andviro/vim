fun! VimExec(l1, l2)
  let l:l1 = a:l1
  while l:l1 <= a:l2
    let l:s = getline(l:l1)
    let l:cmd = substitute(l:s, '^[^:]*\(.*\)', '\1', '')
    silent! exec l:cmd
    let l:l1 = l:l1 + 1
  endwhile
endfun

command! -range VimExec call VimExec(<line1>, <line2>)
