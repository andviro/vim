if !has('python')
    finish
endif

python <<EOF
import re
from tempfile import TemporaryFile
from datetime import datetime
try:
    from subprocess import check_output, CalledProcessError
    has_check_output = True
except ImportError:
    has_check_output = False
    
    

def run_helper(*args):
    if not has_check_output:
        return 0, '', ''
    err = TemporaryFile()
    starttime = datetime.now()
    try:
        out, code = check_output(args, stderr = err), 0
    except CalledProcessError, ex:
        out, code = ex.output, ex.returncode
    except OSError as oserr:
        out, code = 'Error: {0}'.format(oserr), -1
    delta = datetime.now() - starttime
    err.seek(0)
    return code, '=== [started {0}, run time {1}] ===\n{2}\n'.format(starttime, delta, out), str(err.read())
EOF

fun! s:RunHelper(argv)
    pclose | cclose
    let local_efm = &l:efm
    let l:bwin = bufwinnr("***Scratch***")
    if l:bwin == -1
        exec "botright " . &previewheight . "new ***Scratch***"
    else
        exec l:bwin . "wincmd w"
    endif
    let global_efm = &efm
    setlocal buftype=nofile noswapfile nowrap bufhidden=delete
    "setlocal buftype=nofile bufhidden=delete noswapfile nowrap previewwindow
    py args = list(vim.eval('a:argv'))
    py code, out, err = run_helper(*args)
    py lines = out.splitlines()
    py if len(lines): vim.current.buffer.append(out.splitlines(), 0)
    py vim.command("let g:code = %i" % code)
    py vim.command('let g:err = "%s"' % re.escape(err))
    wincmd p
    let &efm = local_efm
    cgetexpr g:err
    let &efm = global_efm
    let &l:efm = local_efm
    if g:code
        exec "belowright " . &previewheight . " cw"
    endif
endfun

fun! s:RunWith(...)
  if &modifiable && &modified | write | endif
  call s:RunHelper(a:000 + [expand('%:p')])
endfun

fun! s:RunSelection(cmplr,l1, l2)
  let l:fn = tempname()
  exec a:l1 . "," . a:l2 . "w! " . l:fn
  call s:RunHelper(a:cmplr + [l:fn])
endfun

command! -nargs=* -complete=shellcmd RunCmd :call s:RunHelper([<f-args>])
command! -nargs=* -complete=shellcmd RunWith :call s:RunWith(<f-args>)
command! -nargs=* -complete=shellcmd -range RunSelection :call s:RunSelection([<f-args>], <line1>, <line2>)
