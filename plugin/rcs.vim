"$Id: rcs.vim,v 1.15.1.19 2003/08/04 13:08:44 andrew Exp $"
augroup RCS
    au!
    au BufNewFile * call RCS_get("")
    au BufUnload * call RCS_checkin(expand('<afile>'), '*** autocommit ***')
    au BufEnter * call RCS_mkmenu()
    au BufLeave * silent! aunmenu RCS
augroup END

fun! RCS_managed(fname)
    let path = fnamemodify(a:fname, ':p:h')
    let basename = fnamemodify(a:fname, ':t')
    return isdirectory(path . '/RCS') &&
                \ filereadable(path . '/RCS/' . basename . ',v') ||
                \ filereadable(a:fname . ',v')
endfun

fun! RCS_mkmenu()
    let fname = expand('%:p')
    silent! aunmenu RCS
    amenu <silent> &RCS.&Refresh\ menu :call RCS_refresh()<CR>
    if !RCS_managed(fname)
        if buflisted(fname)
            amenu <silent> &RCS.&Import\ current :call RCS_import()<CR>
        endif
        return
    endif
    amenu <silent> &RCS.&Commit\ changes :call RCS_commit()<CR>
    if !exists('b:RCS_revision_list')
        call RCS_mkrevlist(fname)
    endif
    let t = b:RCS_revision_list
    while strlen(t)
        let r = matchend(t, '[0-9.]\+\n')
        if r == -1
            break
        endif
        let rev = strpart(t, 0, r-1)
        let t = strpart(t, r)
        exe 'amenu <silent> &RCS.R&evert\ to\ revision.' . escape(rev, '.') . ' :call RCS_revert("' . rev . '")<CR>'
        exe 'amenu <silent> &RCS.View\ &differences\ with.' . escape(rev, '.') . ' :call RCS_diff("' . rev . '")<CR>'
    endwhile
endfun

fun! RCS_commit(...)
    if a:0 == 0
        let msg = "*** autocommit ***"
    else
        let msg = a:1
    endif
    silent! unlet b:RCS_revision_list
    call RCS_checkin(expand('%'), msg)
    call RCS_mkmenu()
endfun

fun! RCS_mkrevlist(fname)
    let t = system("rlog " . a:fname)
    let t = substitute(t, '.\{-}revision\s*\([0-9.]\+\).\{-}', '\1\n', 'g')
    let b:RCS_revision_list = substitute(t, '.\{-}\(\([0-9.]\+\n\)*\).*', '\1', 'g')
endfun

fun! RCS_refresh()
    silent! unlet b:RCS_revision_list
    call RCS_mkmenu()
endfun

fun! RCS_import()
    let fname = expand('%:p')
    if !RCS_managed(fname)
        update
        call system('ci -q -i -l -t-"*** imported by Vim RCS plugin ***" ' . fname)
        doau RCS BufEnter
    endif
endfun

fun! RCS_get(rev)
    let fname = expand('%:p')
    if RCS_managed(fname)
        call system('co -q -f -l -r' . a:rev . ' ' . fname)
        if v:shell_error == 0
            e!
            syn on
        endif
        silent! unlet b:RCS_revision_list
        doau RCS BufEnter
    endif
endfun

fun! RCS_revert(rev)
    let fname = expand('%:p')
    if RCS_managed(fname)
        update
        call system('ci -q -m"*** abandoned branch tip by Vim RCS plugin ***" ' . fname)
        call RCS_get(a:rev)
    endif
endfun

fun! RCS_diff(rev)
    update
    let fname = expand('%:p')
    exe 'sp ' . tempname() . '.diff'
    setlocal bufhidden=wipe filetype=diff
    exe 'r!rcsdiff -q -u -r' . a:rev . ' ' . fname
    setlocal nomodifiable nomodified
    redraw
endfun

fun! RCS_checkin(fname, msg)
    if RCS_managed(a:fname)
        call system('ci -q -l -m"' . escape(a:msg, '"') . '" ' . a:fname)
    endif
endfun

fun! RCS_complete(alead, cmdl, cpos)
    if !exists('b:RCS_revision_list')
        call RCS_mkrevlist(expand('%:p'))
    endif
    return b:RCS_revision_list
endfun

command! -nargs=1 -complete=custom,RCS_complete RCSDiff call RCS_diff(<f-args>)
command! -nargs=1 -complete=custom,RCS_complete RCSRevert call RCS_revert(<f-args>)
command! -nargs=? RCSCommit call RCS_commit(<f-args>)
command! RCSImport call RCS_import()

