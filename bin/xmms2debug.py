#!/usr/bin/env python
import xmmsclient
import os, sys, time, threading


cbTimers = {}
def scheduleCb(cbck, name, arg, t=0.3):
    if name in cbTimers:
        cbTimers[name].cancel()
    cbTimers[name] = threading.Timer(t, cbck, [name, arg])
    cbTimers[name].start()

def debugCallback(msg, result=""):
    print msg + ':', str(result)

xmms = xmmsclient.XMMS("xmms2-vim-updater")
try:
	xmms.connect(os.getenv("XMMS_PATH"),
            lambda r : debugCallback("xmms2_exit"))

except IOError, detail:
	print "Connection failed:", detail
	sys.exit(1)

xmms.broadcast_playback_current_id(lambda r : scheduleCb(debugCallback,"playback_current_id", r.value()))
xmms.broadcast_playback_status(lambda r : scheduleCb(debugCallback,"playback_status", r.value()))
xmms.broadcast_playlist_changed(lambda r : scheduleCb(debugCallback,"playlist_changed", r.value()))
xmms.broadcast_playlist_current_pos(lambda r : scheduleCb(debugCallback,"playback_pos", r.value()))
xmms.loop()
