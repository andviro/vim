" Vim Compiler File
" Compiler:	spice

if exists("current_compiler")
    finish
endif
let current_compiler = "spice"

if exists(":CompilerSet") != 2		" older Vim always used :setlocal
  command -nargs=* CompilerSet setlocal <args>
endif

let s:cpo_save = &cpo
set cpo&vim

"CompilerSet makeprg=ngspice\ -b\ %\ -r\ %:r.raw\ &&\ gwave\ %:r.raw
"CompilerSet makeprg=m4\ %\ \>%.temp\ &&\ spice3\ %.temp
"CompilerSet makeprg=ledit\ -h\ ~/.ngspice_history\ \-x\ ngspice\ -qi\ %
CompilerSet makeprg=make\ INFILE=%\ -f\ ~/devel/scripts/make/Makefile.spice

set errorformat=""
"setlocal errorformat=\ %#[%.%#]\ %#%f:%l:%v:%*\\d:%*\\d:\ %t%[%^:]%#:%m,
"    \%A\ %#[%.%#]\ %f:%l:\ %m,%-Z\ %#[%.%#]\ %p^,%C\ %#[%.%#]\ %#%m
"
" ,%-C%.%#

let &cpo = s:cpo_save
unlet s:cpo_save
