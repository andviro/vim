setlocal spell
noremap <buffer> <silent> <F8> :!asciidoc -b docbook % && dblatex -c ~/.dblatex/default.spec %:r.xml && evince %:r.pdf<CR>
noremap <buffer> <silent> <F9> :!asciidoc % && x-www-browser %:r.html<CR>
"noremap <silent> <F9> :!asciidoc %<CR>
"noremap <F9> :make
"imap <buffer> <silent> " <Plug>SmartQuote
