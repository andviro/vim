if exists("b:did_ftplugin")
  finish
endif

compiler saxonb-xquery

if !exists(":RunWith")
  setlocal makeprg=saxonb-xslt\ %
  map <buffer> <F9> :make<CR>
  imap <buffer> <F9> <Esc>:make<CR>
else
  map <buffer> <F9> :RunWith "saxonb-xslt -xsl:%"
  vmap <buffer> <F9> :RunSelection saxonb-xslt<CR>
  imap <buffer> <F9> <Esc>:RunWith saxonb-xslt<CR>
endif

let b:did_ftplugin=1
