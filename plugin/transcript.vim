fun! s:ProcessWith(l1,l2,interp)
    let tf = tempname()
    exec a:l1. "," . a:l2 . "w " . tf
    exec a:l2
    silent put =''
    silent put =''
    exec "r!" . a:interp. " <" . tf
endfun

command! -range -nargs=1 Process call s:ProcessWith(<line1>, <line2>, <args>)

vmap P :Process "python"
