"imap <buffer> <ESC>i <Plug>Tex_InsertItemOnThisLine
"imap <buffer> <ESC>I <Plug>Tex_InsertItemOnNextLine
"imap <buffer> <ESC>B <Plug>Tex_MathBF
"imap <buffer> <ESC>C <Plug>Tex_MathCal
"imap <buffer> <ESC>L <Plug>Tex_LeftRight
noremap <buffer> <Leader>ll :Latexmk<CR>

