if exists("b:did_ftplugin")
  finish
endif

" set ts=2 sw=2 et ft=omlet
set ts=2 sw=2 et
filetype indent on
set cpo-=C
setlocal efm=
      \%AFile\ \"%f\"\\,\ line\ %l\\,\ characters\ %c-%*\\d:,
      \%AFile\ \"%f\"\\,\ line\ %l\\,\ character\ %c:%m,
      \%+EReference\ to\ unbound\ regexp\ name\ %m,
      \%Eocamlyacc:\ e\ -\ line\ %l\ of\ \"%f\"\\,\ %m,
      \%Wocamlyacc:\ w\ -\ %m,
      \%-Zmake%.%#,
      \%C%t%*[^:]:\ %m,
      \%Z

"vmap Q :!camlp4 pa_o.cmo pa_op.cmo pr_o.cmo pr_op.cmo -- -impl -<CR>
"
map <buffer> <F1> :sp /usr/share/doc/ocaml/docs/ocaml-3.11-refman.txt.gz<CR>
if !exists(":RunWith")
  setlocal makeprg=ocaml\ %
  map <buffer> <F9> :make<CR>
  imap <buffer> <F9> <Esc>:make<CR>
else
  map <buffer> <F9> :RunWith ocaml<CR>
  vmap <buffer> <F9> :RunSelection ocaml<CR>
  imap <buffer> <F9> <Esc>:RunWith ocaml<CR>
endif

let b:mw='\<let\>:\<and\>:\(\<in\>\|;;\),'
let b:mw=b:mw . '\<if\>:\<then\>:\<else\>,\<do\>:\<done\>,'
let b:mw=b:mw . '\<\(object\|sig\|struct\|begin\)\>:\<end\>'
let b:match_words=b:mw

let b:did_ftplugin=1
