" Vim syntax file
" Language:	OCaml txt-formatted manual
" Maintainer:	Andrew Rodionoff (arnost AT mail DOT ru)

" For version 5.x: Clear all syntax items
" For version 6.x: Quit when a syntax file was already loaded
if version < 600
  syntax clear
elseif exists("b:current_syntax")
  finish
endif

syn sync minlines=40

" Define the default highlighting.
" For version 5.7 and earlier: only when not done already
" For version 5.8 and later: only when an item doesn't have highlighting yet
if version >= 508 || !exists("did_help_syntax_inits")
  if version < 508
    let did_help_syntax_inits = 1
    command -nargs=+ HiLink hi link <args>
  else
    command -nargs=+ HiLink hi def link <args>
  endif

  syn match ochelpSection '^\s*-\?\s*\(Part: [IVX]\+\|\(Chapter \)\?[0-9]\+\(\.[0-9]\+\)*\)'
  syn match ochelpHref '\[[0-9]\+\(\.[0-9]\+\)*\]'
  syn match ochelpSectionDelim '\*=\(\*=\)*\*'
  syn match ochelpHeadline '^\s*\*\*\+'
  syn region ochelpExample matchgroup=ochelpExampleDelim start='^<<' end='^>>'
  syn match ochelpExampleDelim '^<<\|>>\>'

  HiLink ochelpSection		PreProc
  HiLink ochelpHref	Subtitle
  HiLink ochelpSectionDelim	PreProc
  HiLink ochelpHeadline		Statement
  HiLink ochelpExample	Comment
  HiLink ochelpExampleDelim	Ignore

  HiLink ochelpIgnore		Ignore
  HiLink ochelpHyperTextEntry	String
  HiLink ochelpHeadline		Statement
  HiLink ochelpSectionDelim	PreProc
  HiLink ochelpVim		Identifier
  HiLink ochelpExample		Comment
  HiLink ochelpOption		Type
  HiLink ochelpNotVi		Special
  HiLink ochelpSpecial		Special
  HiLink ochelpNote		Todo
  HiLink ochelpSubtitle		Identifier

  delcommand HiLink
endif

let b:current_syntax = "ocamlhelp"

" vim: ts=8 sw=2
