scriptencoding utf-8

" Initialization "{{{
" ==============
if !exists('g:loaded_my_vimrc')                " don't reset twice on reloading
    let g:loaded_my_vimrc = 1
    set nocompatible
    
    " Vundle init "{{{
    " -----------
    "filetype off
    "set rtp+=~/.vim/bundle/vundle/
    "call vundle#rc()"}}}

    " Operation system-dependent settings "{{{
    " -----------------------------------
    if has("win32") || has("win16")
        set backupdir=~/_cache/vim/backup      " where to put backup file 
        set undodir=~/_cache/vim/undo      " where to put backup file
        set directory=c:/tmp,c:/temp,c:/windows/temp " where to put swap file
        let g:SESSION_DIR   = $HOME.'/_cache/vim/sessions'
    else
        set backupdir=~/.cache/vim/backup      " where to put backup file 
        set undodir=~/.cache/vim/undo      " where to put backup file 
        set directory=/tmp " where to put swap file
        let g:SESSION_DIR = $HOME.'/.cache/vim/sessions'
        set path+=/usr/include,/usr/local/include,/usr/X11R6/include,$HOME/texmf/tex
    endif"}}}

    " Create system vim dirs "{{{
    " ----------------------
        if finddir(&backupdir) == ''
        silent call mkdir(&backupdir, "p")
        endif

        if finddir(&undodir) == ''
        silent call mkdir(&undodir, "p")
        endif

        if finddir(g:SESSION_DIR) == ''
        silent call mkdir(g:SESSION_DIR, "p")
        endif"}}}
    " Load bundles "{{{
    " ------------
    call plug#begin('~/.vim/bundle')

        "Plug 'gmarik/vundle'
        Plug 'kongo2002/fsharp-vim'
        "Plug 'plasticboy/vim-markdown'
        Plug 'vim-pandoc/vim-pandoc'
        Plug 'vim-pandoc/vim-pandoc-syntax'
        Plug 'vim-pandoc/vim-pandoc-after'
        Plug 'kchmck/vim-coffee-script'
        Plug 'marijnh/tern_for_vim', { 'do': 'npm update' }
        Plug 'othree/tern_for_vim_coffee'
        Plug 'mtscout6/vim-cjsx'
        Plug 'chrisgillis/vim-bootstrap3-snippets'
        Plug 'groenewege/vim-less'
        "Plug 'mattn/emmet-vim'
        "Plug 'wombat256.vim'
        Plug 'digitaltoad/vim-jade'
        Plug 'andviro/vim-colors-solarized'
        "Plug 'Rip-Rip/clang_complete'
        "Plug 'justmao945/vim-clang'
        Plug 'Valloric/YouCompleteMe', { 'do': './install.py --clang-completer --gocode-completer' }
        "Plug 'Lokaltog/powerline'
        Plug 'bling/vim-airline'
        "Plug 'itchyny/lightline.vim'
        "Plug 'andviro/vim-bufferline'
        Plug 'airblade/vim-gitgutter'
        Plug 'kopischke/vim-stay'
        "Plug 'mhinz/vim-signify'
        "Plug 'Lokaltog/vim-powerline'
        Plug 'simnalamburt/vim-mundo'
        "Plug 'dahu/MarkMyWords'
        Plug 'Raimondi/delimitMate'
        "Plug 'spf13/vim-autoclose'
        Plug 'tpope/vim-fugitive'
        Plug 'tpope/vim-git'
        Plug 'davidhalter/jedi-vim'
        "Plug 'andviro/jedi-vim'
        "Plug 'andviro/ropevim-bundled'
        Plug 'andviro/flake8-vim'
        Plug 'elzr/vim-json'
        "Plug 'andviro/pylint-mode'
        Plug 'phleet/vim-mercenary'
        Plug 'hynek/vim-python-pep8-indent'
        "Plug 'andviro/Efficient-python-folding'
        "Plug 'andviro/python-mode'
        "Plug 'ujihisa/neco-ghc'
        "Plug 'Tabular'
        "Plug 'ajh17/VimCompletesMe'
        Plug 'scrooloose/nerdcommenter'
        Plug 'scrooloose/nerdtree'
        Plug 'ctrlpvim/ctrlp.vim'
        "Plug 'Shougo/neocomplete.vim'
        "Plug 'Shougo/neomru.vim'
        "Plug 'Shougo/neossh.vim'
        "Plug 'Shougo/unite.vim'
        "Plug 'Shougo/unite-session'
        "Plug 'xolox/vim-session'
        "Plug 'xolox/vim-misc'
        "Plug 'andviro/vim-unite-projects'
        "Plug 'Shougo/vimproc', { 'do': 'make' }
        "Plug 'tpope/vim-obsession'
        "Plug 'dhruvasagar/vim-prosession'
        "Plug 'jeetsukumaran/vim-buffergator'
        "Plug 'jeetsukumaran/vim-filebeagle'
        "Plug 'Shougo/vimfiler'
        "Plug 'utl.vim'
        "Plug 'manuel-colmenero/vim-simple-session'
        Plug 'vimwiki'
        "Plug 'ervandew/supertab'
        Plug 'sirver/ultisnips'
        Plug 'honza/vim-snippets'
        "Plug 'LaTeX-Help'
        "Plug 'FuzzyFinder'
        Plug 'Lokaltog/vim-easymotion'
        "Plug 'majutsushi/tagbar'
        "Plug 'xmledit'
        "Plug 'sukima/xmledit'
        "Plug 'spf13/PIV'
        "Plug 'Rykka/riv.vim'
        Plug 'Glench/Vim-Jinja2-Syntax'
        "Plug 'tristen/vim-sparkup'
        Plug 'fatih/vim-go'
        "Plug 'surround.vim'
        Plug 'matchit.zip'
        Plug 'asciidoc.vim'
        Plug 'AutoFenc'
        Plug 'SWIG-syntax'
        "Plug 'Conque-Shell'
        Plug 'pangloss/vim-javascript', {'branch' : 'develop'}
        Plug 'terryma/vim-multiple-cursors'
        Plug 'benekastah/neomake'
        Plug 'Chiel92/vim-autoformat'
        Plug 'rdnetto/YCM-Generator', {'branch' : 'stable'}
        Plug 'def-lkb/vimbufsync'
        Plug 'yosssi/vim-ace'
        Plug 'stephpy/vim-yaml'
        Plug 'chase/vim-ansible-yaml'
        Plug 'wincent/terminus'
        Plug 'christoomey/vim-tmux-runner'
        "Plug 'aperezdc/vim-template'
        Plug 'noahfrederick/vim-skeleton'
        "Plug 'andviro/nim.vim'
    call plug#end()

        filetype plugin on
        filetype indent on
        syntax on
    "}}}
endif"}}}


" Windows/Linux portability "{{{
" =========================
    if has("win32") || has("win16")
        lang mes C
        lang ctype Russian_Russia.1251
        set viminfo='100,<100,f1,h,:50,/50,n~/_viminfo
    else
        set viminfo='100,<100,f1,h,:50,/50
        " Man plugin "{{{
        " ----------
        so $VIMRUNTIME/ftplugin/man.vim
        fun! ManComplete(A,L,P)
            if a:L =~ '^\s*[^ ]\+\s\+[^ ]\s\+'
            let l:section = substitute(a:L, '^\s*[^ ]\+\s\+\([^ ]\).*', '\1', '')
            else
            let l:section = '*'
            endif
            let l:mp = globpath('/man,/usr/man,/usr/X11R6/man,
                \/usr/share/man,/usr/local/man', 'man' . l:section)
            let l:mpp = substitute(l:mp, "\n", ',', 'g')
            let l:allman = globpath(l:mpp, a:A . '*')
            let l:allpages = substitute(l:allman, '.\{-}/\([^/.]*\)[^/]\{-}\(\n\|$\)',
                \ '\1\2', 'g')
            return l:allpages
        endfun
    
        com! -nargs=* -complete=custom,ManComplete MMan Man <args>
    
        nmap <silent> K :exec 'Man ' . expand('<cword>')<CR>
        nmap <C-K> :Info <cword><CR>"}}}
    endif
"}}}

" Appearance "{{{
" ==========
    set background=dark
    "colorscheme wombat256mod
    let g:solarized_termcolors = 16
    let g:solarized_visibility = "normal"
    let g:solarized_contrast = "high"
    colorscheme solarized

    if has("win32") || has("win16")
        set guifont=Lucida_Console:h18:cRUSSIAN
        set lines=42 columns=135
    elseif has("gui_running")    
        set guifont=Monospace\ 16
    endif
    set guioptions=egc
    set cursorline
    hi! link VertSplit StatusLine
    "hi Visual ctermbg=237
    hi! Cursor guibg=red guifg=black
    let hs_highlight_more_types = 1
    let hs_highlight_types = 1
    let hs_highlight_boolean = 1
    let hs_highlight_delimiters = 1
    hi notesItalic gui=italic cterm=italic
    hi notesBold gui=bold cterm=bold"
    hi Comment gui=bold
    "hi String gui=none
    hi StatusLine gui=bold
    hi VertSplit gui=bold
    "hi Normal ctermbg=none ctermfg=none
    "}}}

" Settings "{{{
" ========
    set fileencodings=utf-8,cp1251,koi8-r
    set undofile
    set mouse=ar
    set title
    set runtimepath+=~/.vim
    set viewdir=~/.vim/views
    set viewoptions=cursor,folds,slash,unix
    "set tags+=$HOME/tags/main,$HOME/tags/x,$HOME/tags/local
    set virtualedit=block
    set swapsync=

    set keymap=russian-yawerty
    set iminsert=0
    set imsearch=-1
    set spelllang=ru,en
    set cmdheight=1
    set scrolloff=3
    set autoindent
    set diffopt+=vertical
    set infercase
    set dictionary+=$HOME/dict/dict
    "let &iconstring="%k%F %m - %{v:servername}"
    "let &titlestring=&iconstring
    set list
    set listchars=tab:>-,eol:�,nbsp:~
    set novisualbell
    set number

    set backup
    set history=50 " keep 50 lines of command line history
    set sessionoptions-=blank,options
    "set sessionoptions+=localoptions,unix
    set ruler " show the cursor position all the time
    set rulerformat=
    set laststatus=2
    set nowildmenu
    "set wildmode=longest,list,full
    set wildmode=list,longest:list,full
    set wildignore+=RCS,CVS,*~,*.aux,*.bak,*.dvi,*.toc
    set wildignore+=*.idx,*.log,*.swp,*.tar,*.o,*.cm?,*.d
    set wildignore+=*.haux,*.htoc,*.image.tex,*.pyc,*.out,*\\,v
    set wildignore+=*.bbl,*.blg,*.out
    set wildignore+=.git,.hg,*.svn
    set wildignore+=*.sqlite
    "set wildignore+=*\\.git\\*,*\\.hg\\*,*\\.svn\\*  " Windows ('noshellslash')
    set wildcharm=<C-Z>
    set report=0
    set hidden
    set autoread
    set autowrite
    set showmatch
    set showcmd
    set noshowmode
    set suffixes+=.txt,.ps,.pdf,.html,.obj,.eps,.doc,.gz,.tar,.zip,.bz2,.dat

    set nolazyredraw
    set incsearch
    set smartcase
    set ignorecase
    set nohlsearch

    set nowrap
    set joinspaces
    set tabstop=4
    set softtabstop=4
    set shiftwidth=4
    set shiftround
    set expandtab
    set textwidth=79
    set foldcolumn=0
    set foldmethod=marker
    set foldtext=v:folddashes.substitute(getline(v:foldstart),'/\\*\\\|\\*/\\\|[{]\\{3}\\d\\=','','g')
    set foldlevel=99
    set foldlevelstart=99
    set complete=.,w,b,k
    set completeopt=menuone,longest

    "set statusline=%{BufStatus()}%<%f[%{expand('#:p:.')}]\ %2*%{strftime('%b\ %d\ %H:%M')}\ %3*%k\ %*%([%1*%H%*]%)%([%1*%Y%*]%)%([%2*%R%*]%)%([%2*%M%*]%)%([%2*%W%*]%)%=%-16(\ %l,%c%((%v)%)%)%P
    "set statusline=%f%(\|%2*%{expand('#:p:.')}%*%)\ %3*%k\ %*%([%1*%H%*]%)%([%1*%Y%*]%)%([%2*%R%*]%)%([%2*%M%*]%)%([%2*%W%*]%)%=%-16(\ %l,%c%((%v)%)%)%P
    "set statusline=%0.60(%f\ \|\ %0.40{expand('#:p:.')}%*%)\ %k\ %*%([%H%*]%)%([%Y%*]%)%([%R%*]%)%([%M%*]%)%([%W%*]%)%=%-16(\ %l,%c%((%v)%)%)%P

    if has('unnamedplus')
        set clipboard=autoselect,unnamedplus
    elseif has('nvim')
        set clipboard=unnamedplus
    elseif has('x')
        set clipboard=autoselect,unnamed
    elseif has('gui')
        set clipboard=autoselect,unnamed
        set guioptions+=a
    endif

    set nostartofline
    set splitbelow
    set noequalalways
    set helpheight=10
    set whichwrap=<,>,h,l

    if executable("ag")
        set grepprg=ag\ --nogroup\ --nocolor
    else
        set grepprg=grep\ -nH\ $*
    endif
"}}}

" Tabs setup "{{{
" ----------
if 0
    function! MyTabLine()
    let s = ''
    for i in range(tabpagenr('$'))
        " select the highlighting
        if i + 1 == tabpagenr()
        let s .= '%#TabLineSel#'
        else
        let s .= '%#TabLine#'
        endif

        " set the tab page number (for mouse clicks)
        let s .= '%' . (i + 1) . 'T'

        " the label is made by MyTabLabel()
        let s .= ' %{MyTabLabel(' . (i + 1) . ')} '
    endfor

    " after the last tab fill with TabLineFill and reset tab page nr
    let s .= '%#TabLineFill#%T'

    " right-align the label to close the current tab page
    if tabpagenr('$') > 1
        let s .= '%=%#TabLine#%999Xclose'
    endif

    return s
    endfunction

    function! MyTabLabel(n)
    let buflist = tabpagebuflist(a:n)
    let winnr = tabpagewinnr(a:n)
    return bufname(buflist[winnr - 1])
    endfunction

    set tabline=%!MyTabLine()
    "set stal=2
    "set iskeyword+=:
    let g:tex_flavor='latex'
    set previewheight=6
endif
"}}}

" Functions "{{{
" =========
"
    " Key bind helper
    fun! rc#Map_ex_cmd(key, cmd)  
      execute "nmap ".a:key." " . ":".a:cmd."<CR>"
      execute "cmap ".a:key." " . "<C-C>:".a:cmd."<CR>"
      execute "imap ".a:key." " . "<C-O>:".a:cmd."<CR>"
      execute "vmap ".a:key." " . "<Esc>:".a:cmd."<CR>gv"
    endfun  

    fun! rc#RunFileExplorer(stay)
        VimFilerExplorer -project -find -auto-expand -winwidth=20
        if !a:stay
            exe "normal \<C-w>\<C-p>"
        endif
    endfun

    " Option switcher helper
    fun! rc#Toggle_option(key, opt)  
      call rc#Map_ex_cmd(a:key, "set ".a:opt."! ".a:opt."?")
    endfun  

    " Sessions
    " XXX moved to unite-sessions

    " Omni and dict completition
    fun! rc#AddWrapper()  
        if exists('&omnifunc') && &omnifunc != ''
            return "\<C-X>\<C-o>\<C-p>"
        else
            return "\<C-N>"
        endif
    endfun  

    " Recursive vimgrep
    fun! rc#RGrep()  
        let pattern = input("Search for pattern: ", expand("<cword>"))
        if pattern == ""
            return
        endif

        let cwd = getcwd()
        let startdir = input("Start searching from directory: ", cwd, "dir")
        if startdir == ""
            return
        endif

        let filepattern = input("Search in files matching pattern: ", "*.*") 
        if filepattern == ""
            return
        endif

        execute 'noautocmd vimgrep /'.pattern.'/gj '.startdir.'/**/'.filepattern | botright copen
    endfun  

    " Restore cursor position
    fun! rc#restore_cursor() 
        if line("'\"") <= line("$")
            silent! normal! g`"
            return 1
        endif
    endfunction 

    " Simple one-key menu prompt
    fun! rc#hotkeyMenu(prefix, menudef)
        let l:prompt = ''
        for [key, value] in items(a:menudef)
            let l:prompt .= value[0] . ' '
        endfor
        let l:prompt .= '?'
        echon '[' . a:prefix . '] ' . l:prompt
        let l:cmd = nr2char(getchar())
        if !has_key(a:menudef, l:cmd)
            return
        endif
        exec a:menudef[l:cmd][1]
    endfunction

    function! rc#CmdLine(str)
        exe "menu Foo.Bar :" . a:str
        emenu Foo.Bar
        unmenu Foo
    endfunction

    function! rc#VisualSelection(direction) range
        let l:saved_reg = @"
        execute "normal! vgvy"

        let l:pattern = escape(@", '\\/.*$^~[]')
        let l:pattern = substitute(l:pattern, "\n$", "", "")

        if a:direction == 'b'
            execute "normal ?" . l:pattern . "^M"
        elseif a:direction == 'gv'
            call rc#CmdLine("vimgrep " . '/'. l:pattern . '/' . ' **/*.')
        elseif a:direction == 'replace'
            call rc#CmdLine("%s" . '/'. l:pattern . '/')
        elseif a:direction == 'f'
            execute "normal /" . l:pattern . "^M"
        endif

        let @/ = l:pattern
        let @" = l:saved_reg
    endfunction

""}}}

" Mappings "{{{
" ========
    let mapleader = ';'

" Visual mode {{{
" -----------
    vnoremap Q gq
    "vnoremap < <gv
    "vnoremap > >gv
    " Visual mode pressing * or # searches for the current selection
    " Super useful! From an idea by Michael Naumann
    vnoremap <silent> * :call rc#VisualSelection('f')<CR>
    vnoremap <silent> # :call rc#VisualSelection('b')<CR>
    """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    " When you press gv you vimgrep after the selected text
    vnoremap <silent> gv :call rc#VisualSelection('gv')<CR>

    " Open vimgrep and put the cursor in the right position
    "map <leader>g :vimgrep // **/*.<left><left><left><left><left><left><left>

    " Vimgreps in the current file
    "map <leader><space> :vimgrep // <C-R>%<C-A><right><right><right><right><right><right><right><right><right>

     "When you press <leader>R you can search and replace the selected text
    vnoremap <silent> <leader>R :call rc#VisualSelection('replace')<CR>
"}}}

" Insert mode {{{
" ------------
    inoremap <C-E> <C-o>A
    inoremap <C-A> <C-o>I
    inoremap <C-BS> <C-W>
"}}}
    
" Normal mode {{{
" ------------

    " Nice scrolling if line wrap
    noremap j gj
    noremap k gk

    " Split line in current cursor position
    "noremap <CR>       i<CR><ESC>

    " Fast scroll
    nnoremap <C-e> 3<C-e>
    nnoremap <C-y> 3<C-y>
    map <Space> <C-D>

    map!  <BS>
    map! <C-H> <BS>

    " Select all
    map vA ggVG

    " Quickfix fast navigation
    nnoremap <silent> <Leader>nn :cwindow<CR>:cn<CR>
    nnoremap <silent> <Leader>pp :cwindow<CR>:cp<CR>

    " Buffer commands
    "
    " Search the current file for the word under the cursor and display matches
    nnoremap <silent> ,gw :call rc#RGrep()<CR>


    " Session UI
    "nnoremap <Leader>ss :<C-u>Unite session/new<CR>

    " Quick grep
    nnoremap <Leader>s :grep! "\b<C-R><C-W>\b"<CR>:cw<CR>


    " Show syntax highlighting groups for word under cursor
    nmap <Leader>S :call <SID>SynStack()<CR>
    function! <SID>SynStack()
        if !exists("*synstack")
            return
        endif
        echo map(synstack(line('.'), col('.')), 'synIDattr(v:val, "name")')
    endfunc

    " screen refresh
    nnoremap <Esc>l <C-L>
    nnoremap <M-L> <C-L>
    
    " disable ex mode
    map Q <nop>

    " buffer switch
    nmap <C-H> :bp<CR>
    nmap <C-L> :bn<CR>
    "nmap <BS> :b#<CR>
    "nmap  :b#<CR>
    nmap <Tab> :b#<CR>
    "nmap <Tab> :BuffergatorToggle<CR>
    "nmap <Tab> :TagbarToggle<CR>
    "nnoremap <Tab> :CtrlPBuffer<CR>

    " window commands
    noremap <C-W>o :only<CR>
    map <C-W>q :bw<CR>
    "nnoremap <C-j> <C-W>j
    "nnoremap <C-k> <C-W>k
    "nnoremap <C-h> <C-W>h
    "nnoremap <C-l> <C-W>l

    "nmap <C-j> <C-W>j
    "nmap <C-k> <C-W>k

    " jump history
    nnoremap L <C-I>
    nnoremap H <C-O>
    nnoremap <BS> <C-O>
    " tabs
    
    "call rc#Map_ex_cmd("<C-j>", "tabNext")
    nnoremap <C-j> :tabnext<CR>
    "call rc#Map_ex_cmd("<C-k>", "tabprevious")
    nnoremap <C-k> :tabprevious<CR>

    " Neovim terminal
    if has("nvim")
        tnoremap <Esc> <C-\><C-n>
    endif


    function! g:JInMenu()
        if pumvisible()
            return "\<C-n>"
        else
            return "\<c-j>"
        endif
    endfunction

    function! g:KInMenu()
        if pumvisible()
            return "\<C-p>"
        else
            return "\<c-k>"
        endif
    endfunction

    inoremap <c-j> <c-r>=g:JInMenu()<cr>
    inoremap <c-k> <c-r>=g:KInMenu()<cr>

    map <Leader>h :e %:p:s,.h$,.X123X,:s,.cpp$,.h,:s,.X123X$,.cpp,<CR>


    call rc#Map_ex_cmd("<Leader><C-j>", "tabmove")
    call rc#Map_ex_cmd("<Leader><C-k>", "tabmove -1")
" }}}

" Emacs-like commandline edit {{{
" ---------------------------
    " start of line
    cnoremap <C-A>      <Home>
    " back one character
    cnoremap <C-B>      <Left>
    inoremap <C-B>      <Left>
    " delete character under cursor
    cnoremap <C-D>      <Del>
    " end of line
    cnoremap <C-E>      <End>
    " forward one character
    cnoremap <C-F>      <Right>
    inoremap <C-F>      <Right>
    " recall newer command-line
    cnoremap <C-N>      <Down>
    " recall previous (older) command-line
    cnoremap <C-P>      <Up>
    " back one word
    cnoremap <Esc><C-B> <S-Left>
    " forward one word
    cnoremap <Esc><C-F> <S-Right>
    "}}}
    
" Keymap switching {{{
" ----------------
    imap <C-\> <C-^>
    imap <C-_> <C-^>
    imap <C-Space> <C-^>
    cmap <C-\> <C-^>
    cmap <C-_> <C-^>
    cmap <C-Space> <C-^>
    " This is a dirty trick to make keymap switch available in normal mode
    nmap <silent> <C-\> :let &l:iminsert = !&l:iminsert<CR>
    nmap <silent> <C-/> :let &l:iminsert = !&l:iminsert<CR>
    nmap <silent> <C-_> :let &l:iminsert = !&l:iminsert<CR>
    nmap <silent> <C-Space> :let &l:iminsert = !&l:iminsert<CR>
"}}}

"}}}

" Commands "{{{
" ========
    command! -nargs=* -complete=file E :e <args>
    command! -nargs=* -complete=file W :w <args>
    command! Q :q
    command! Qa :qa
"}}}

" Autocommands "{{{
" ============
    augroup vimrc
    au!
        "au VimEnter * colorscheme solarized
        " Auto-open VimFiler
        "au vimenter * :call rc#RunFileExplorer(0)
        autocmd FileType vimfiler setlocal nonumber
        " Auto-find file
        "au BufEnter * VimFilerExplorer -find | exe "normal \<c-w>\<c-p>"

        au QuickfixCmdPost make :cwin 5

        " Auto reload vim settins
        au BufWritePost *.vim source ~/.vim/rc.vim

        au BufWritePost ~/.Xdefaults !xrdb %
        au BufWritePost ~/.fvwm/.fvwm2rc.menu !FvwmCommand "Read %"


        " Highlight insert mode
        "au InsertEnter * set cursorline
        "au InsertLeave * set nocursorline
        

        " Restore cursor position
        au BufWinEnter * call rc#restore_cursor()
        "au BufReadPost * if line("'\"") && line("'\"") <= line("$") |
                "\ exe "'\"" |
                "\ endif

        " Autosave last session
        "if has('mksession') 
            "au VimLeavePre * :UniteSessionSave
        "endif

        " Save current open file when window focus is lost
        au FocusLost * if &modifiable && &modified | write | endif

        " Filetypes 
        " ---------
        
        au BufNewFile,BufRead *.json setf javascript
        "au BufNewFile,BufRead *.html setlocal ft=jinja
        au BufNewFile,BufRead *.j2 setlocal ft=jinja
        au BufNewFile,BufRead *.tag setlocal ft=html
        au BufRead,BufNewFile *.txt setlocal ft=asciidoc
        au BufRead *.hva setlocal ft=tex
        au BufWrite *.html :Autoformat
        " 
        " Auto close preview window
        autocmd CursorMovedI * if pumvisible() == 0|pclose|endif 
        autocmd InsertLeave * if pumvisible() == 0|pclose|endif
    augroup END"}}}

" Plugin-specific settings and maps "{{{
" =================================

" NERD Commenter "{{{
" --------------
    vmap <silent> K <plug>NERDCommenterInvert
    let g:NERDCustomDelimiters = {
        \ 'spice': { 'left': '* ' },
        \ 'fsharp': { 'left': '// ' },
        \ 'python': { 'left': '# ' },
        \ 'snippets': { 'left': '# ' },
        \ 'jinja': { 'left': '{# ', 'right': ' #}' }
    \ }"}}}

" NERD Tree "{{{
" ---------
if 1
    "call rc#Map_ex_cmd("<tab>", "NERDTreeToggle")
    call rc#Map_ex_cmd("<Leader><Tab>", "NERDTreeFind")
    "nnoremap <silent> <BS> :NERDTreeFind<CR>
    "nnoremap <silent>  :NERDTreeFind<CR>
    "nnoremap <silent> <leader>f :NERDTreeFind<CR>
    let g:NERDTreeWinPos = 'left'
    let g:NERDTreeWinSize=20
    let g:NERDTreeQuitOnOpen = 0
    let g:NERDTreeIgnore=[
        \'\~$',
        \'\.swp$',
        \'\.beam$',
        \'build$',
        \'dist$',
        \'\.DS_Store$',
        \'\.egg$',
        \'\.egg-info$',
        \'\.la$',
        \'\.lo$',
        \'\.\~lock.*#$',
        \'\.mo$',
        \'\.o$',
        \'\.pt.cache$',
        \'\.py[cow]$',
        \'__pycache__$',
        \'\.Python$',
        \'\..*.rej$',
        \'\.rej$',
        \'\.ropeproject$',
        \'\.svn$',
        \'\.hg$',
        \'\.git$',
        \'\.tags$'
    \]
else " VimFiler
    let g:vimfiler_as_default_explorer = 1
    nnoremap <silent> <Leader><Tab> :call rc#RunFileExplorer(1)<CR>
endif
"}}}

" GUndo "{{{
" -----
    map <Leader>u :GundoToggle<CR>
"}}}

" SuperTab "{{{
" --------
    let g:SuperTabDefaultCompletionType = "context"
    "let g:SuperTabCompletionContexts = ['s:ContextText', 's:ContextDiscover']
    "let g:SuperTabContextTextOmniPrecedence = ['&omnifunc', '&completefunc']
    "let g:SuperTabContextDiscoverDiscovery =
    "    \ ["&completefunc:<c-x><c-p>", "&omnifunc:<c-x><c-o>"]
    "let g:LatexBox_latexmk_options="-ps"
"}}}

" PythonMode "{{{
" ----------
    let g:vim_program="/usr/bin/vim"
    let g:pymode_syntax=1
    let g:pymode_doc_key='K'
    let g:pymode_options_other=0
    let g:pymode_lint_maxheight=4
    let g:pymode_rope_vim_completion=1
    let g:pymode_rope_extended_complete=1
    " let g:pymode_lint = 0
    " let g:pymode_run_key = '<F9>'
    "}}}

" Fugitive "{{{
" --------
if 1
    let s:fugitive_menu = {
                \ 's' : ['(s)tatus', 'Git status'],
                \ 'a' : ['(a)dd', 'exe "Git add " . expand("%")'],
                \ 'c' : ['(c)ommit', 'Gcommit'],
                \ 'C' : ['(C)ommit', 'Gcommit -a'],
                \ 'd' : ['(d)iff', 'Gdiff'],
                \ 'l' : ['(l)log', 'silent Glog -- | copen'],
                \ 'h' : ['(h)ist', 'silent Git hist'],
                \ 'u' : ['p(u)ll', 'Git pull'],
                \ 'p' : ['(p)ush', 'Git push'], }
    command! FugitiveMenu :call rc#hotkeyMenu(fugitive#head(6), s:fugitive_menu)
    nnoremap <leader>g :FugitiveMenu<CR>
else
    let g:unite_source_menu_menus = {}
    let g:unite_source_menu_menus.fugitive = {
        \     'description' : 'fugitive menu',
        \ }
    let g:unite_source_menu_menus.fugitive.candidates = {
        \       'add' : 'Gwrite',
        \       'blame' : 'Gblame',
        \       'diff' : 'Gdiff',
        \       'commit' : 'Gcommit',
        \       'Commit -a' : 'Gcommit -a',
        \       'status' : 'Gstatus',
        \       'hist' : 'Git hist',
        \     }

    function g:unite_source_menu_menus.fugitive.map(key, value)
    return {
            \       'word' : a:key, 'kind' : 'command',
            \       'action__command' : a:value,
            \     }
    endfunction
    nnoremap <silent> <Leader>g :<C-u>Unite -start-insert menu:fugitive<CR>
endif

"}}}

" UltiSnips "{{{
" ---------
    let g:UltiSnipsExpandTrigger="<tab>"
    let g:UltiSnipsJumpForwardTrigger="<tab>"
    let g:UltiSnipsJumpBackwardTrigger="<s-tab>"
    let g:notes_directory = expand('~/Dropbox/notes')
    "let g:UltiSnipsSnippetsDir = expand('~/.vim/UltiSnips')
"}}}

" Flake8-vim "{{{
" -----
    let g:PyFlakeDisabledMessages = 'E501'
    let g:PyFlakeAggressive = 2
"}}}
"
" CtrlP "{{{
" -----
    let g:ctrlp_key_loop = 1
    let g:ctrlp_by_filename = 0
    let g:ctrlp_match_window_reversed = 0
    let g:ctrlp_reuse_window = 'netrw\|quickfix'
    let g:ctrlp_extensions = ['session']
    let g:ctrlp_working_path_mode = 'ra'
    let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/](\.git|\.hg|\.svn|bower_components|node_modules)$'
    \ }
    " Multiple VCS's:
    if 0
        if executable("ag")
            let s:ctrlp_fallback = 'ag %s -l --nocolor -g ""'
        else
            let s:ctrlp_fallback = 'find %s -type f'
        endif
        let g:ctrlp_user_command = {
            \ 'types': {
            \ 1: ['.git', 'cd %s && git ls-files'],
            \ 2: ['.hg', 'hg --cwd %s locate -I .'],
            \ },
            \ 'fallback': s:ctrlp_fallback
            \ }
    else
        if executable("ag")
            let g:ctrlp_user_command = 'ag %s -l --nocolor -g ""'
        else
            let g:ctrlp_user_command = 'find %s -type f'
        endif
    endif


    function! CtrlPOpenFunc(action, line)
        let l:fnm = fnamemodify(a:line, ':e')
        let l:fnp = fnameescape(fnamemodify(a:line, ':p'))
        if (l:fnm =~? '^\(html\?\)$' && a:action =~ '^[tx]$')
            \ || l:fnm =~? '^\(doc\|xls\|odt\|ods\)$'
            call ctrlp#exit()
            silent! execute '!xdg-open' l:fnp
            redraw!
        elseif a:action == 'x'
            " Not a HTML file, simulate pressing <c-o> again and wait for new input
            call feedkeys("\<c-o>")
        else
        " Use CtrlP's default file opening function
        call call('ctrlp#acceptfile', [a:action, a:line])
        endif
    endfunction

    let g:ctrlp_open_func = { 'files': 'CtrlPOpenFunc' }
    "let g:ctrlp_cmd = 'CtrlP'
    let g:ctrlp_cmd = 'CtrlPLastMode'
"}}}

" Jedi-vim "{{{
" --------
    let g:jedi#popup_on_dot = 0
    let g:jedi#use_tabs_not_buffers = 0
    let g:jedi#show_call_signatures = 0
    let g:jedi#goto_assignments_command = '<CR>'
    let g:jedi#goto_definitions_command = '<Leader><CR>'
    let g:jedi#rename_command = '<Leader>R'
    let g:jedi#usages_command = '<Leader>n'

    hi link jediFunction Visual
    hi link jediFat VisualNOS
"}}}

" VimWiki "{{{
" -------
    let g:vimwiki_folding = 1
    let g:vimwiki_fold_lists = 1
    let g:vimwiki_list = [
        \ {"path" : expand("~/Dropbox/wiki")},
    \ ]
    nmap <Leader>ww <Plug>VimwikiIndex
    nmap <Leader>wn <Plug>VimwikiNextLink
    nmap <Leader>wp <Plug>VimwikiPrevLink
"}}}
"
" Powerline "{{{
" -------
let g:Powerline_dividers_override = ['', '|', '', '|']
let g:Powerline_colorscheme = 'solarized16'
"let g:Powerline_dividers_override = ['>>', '>', '<<', '<']
"}}}

" Airline "{{{
" -------
let g:airline_left_sep=''
let g:airline_right_sep=''
let g:airline#extensions#whitespace#enabled = 0
"let g:airline#extensions#ctrlp#color_template = 'normal'
let g:airline#extensions#tabline#show_buffers = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#tab_nr_type = 1 " tab number
let g:airline#extensions#tagbar#enabled = 0
let g:airline#extensions#tagbar#flags = 's'
let g:airline#extensions#bufferline#enabled = 0
let g:airline#extensions#tabline#buffer_idx_mode = 1
let g:airline#extensions#ctrlp#show_adjacent_modes = 1
let g:airline_detect_iminsert=1
nmap <leader>1 1gt
nmap <leader>2 2gt
nmap <leader>3 3gt
nmap <leader>4 4gt
nmap <leader>5 5gt
nmap <leader>6 6gt
nmap <leader>7 7gt
nmap <leader>8 8gt
nmap <leader>9 9gt

"}}}
" BufferLine {{{
let g:bufferline_show_bufnr = 0
let g:bufferline_rotate = 2
let g:bufferline_active_buffer_left = '*'
let g:bufferline_active_buffer_right = ' '
let g:bufferline_separator=' '
" }}}
" Lightline {{{
let g:lightline = {
      \ 'colorscheme': 'solarized',
      \ 'mode_map': { 'c': 'NORMAL' },
      \ 'active': {
      \   'left': [ [ 'mode', 'paste' ], [ 'fugitive', 'filename' ] ]
      \ },
      \ 'component_function': {
      \   'modified': 'MyModified',
      \   'readonly': 'MyReadonly',
      \   'fugitive': 'MyFugitive',
      \   'filename': 'MyFilename',
      \   'fileformat': 'MyFileformat',
      \   'filetype': 'MyFiletype',
      \   'fileencoding': 'MyFileencoding',
      \   'mode': 'MyMode',
      \ },
      \ 'separator': { 'left': '', 'right': '' },
      \ 'subseparator': { 'left': '', 'right': '' }
      \ }

function! MyModified()
  return &ft =~ 'help\|vimfiler\|gundo' ? '' : &modified ? '+' : &modifiable ? '' : '-'
endfunction

function! MyReadonly()
  return &ft !~? 'help\|vimfiler\|gundo' && &readonly ? '[RO]' : ''
endfunction

function! MyFilename()
  return ('' != MyReadonly() ? MyReadonly() . ' ' : '') .
        \ (&ft == 'vimfiler' ? vimfiler#get_status_string() : 
        \  &ft == 'unite' ? unite#get_status_string() : 
        \  &ft == 'vimshell' ? vimshell#get_status_string() :
        \ '' != expand('%:t') ? expand('%:t') : '[No Name]') .
        \ ('' != MyModified() ? ' ' . MyModified() : '')
endfunction

function! MyFugitive()
  if &ft !~? 'vimfiler\|gundo' && exists("*fugitive#head")
    let _ = fugitive#head()
    return strlen(_) ? ''._ : ''
  endif
  return ''
endfunction

function! MyFileformat()
  return winwidth(0) > 70 ? &fileformat : ''
endfunction

function! MyFiletype()
  return winwidth(0) > 70 ? (strlen(&filetype) ? &filetype : 'no ft') : ''
endfunction

function! MyFileencoding()
  return winwidth(0) > 70 ? (strlen(&fenc) ? &fenc : &enc) : ''
endfunction

function! MyMode()
  return winwidth(0) > 60 ? lightline#mode() : ''
endfunction

" }}}

" CoffeeScript{{{
    let g:coffee_lint_options = '-f ' . $HOME . '/.vim/coffeelint.json'
    augroup coffeeScript
    au!
    au BufWritePost *.coffee CoffeeLint! | 5cwin
    augroup end
"}}}

" delimitMate "{{{
" -------
    augroup delimitMateGroup
    au!
    au FileType mail,text let b:delimitMate_autoclose = 0
    au FileType python let b:delimitMate_nesting_quotes = ['"', "'"]
    augroup end
"}}}
"
" golang "{{{
" -------
    fun! s:SetupGoLang()
        setlocal nolist nonumber
        nmap <buffer> K <Plug>(go-doc)
        nmap <buffer> <leader>r <Plug>(go-run)
        nmap <buffer> <leader>l :<C-u>Neomake! go_lint<CR>
        nmap <buffer> <leader>t :<C-u>Neomake! go_test<CR>
        "nmap <buffer> <leader>b :<C-u>GoBuild<CR>
        "nmap <buffer> <leader>b :<C-u>silent make\|echo 'Done'<CR>
        nmap <buffer> <CR> <Plug>(go-def)
        nmap <buffer> <leader>n <Plug>(go-referrers)
        nmap <buffer> <leader>R <Plug>(go-rename)
        nmap <buffer> <leader>e <Plug>(go-generate)
        "inoremap <silent> <buffer> . .<C-x><C-o>
        "inoremap <silent> <buffer> ( <C-x><C-o>(
    endf

    augroup goLang
    au!
    au FileType go call <SID>SetupGoLang()
    augroup end
"}}}

" Unite.vim "{{{
if 0
    let g:unite_source_session_enable_auto_save = 1
    let g:unite_source_history_yank_enable = 1
    call unite#filters#matcher_default#use(['matcher_fuzzy'])
    call unite#custom#profile('default', 'context', {
      \ 'winheight': 12,
      \ 'direction': 'botright',
      \ 'prompt_direction': 'top'
      \ })

    "call unite#custom#source('file,file_rec,buffer', 'sorters', ['sorter_length'])
	call unite#filters#sorter_default#use(['sorter_length'])
    call unite#custom#source('file_mru', 'sorters', ['sorter_ftime','sorter_reverse'])

    "nnoremap <C-p> :<C-u>Unite -no-split -buffer-name=files   -start-insert file_rec/async:!<cr>
    "nnoremap <C-p> :<C-u>Unite  -buffer-name=files   -start-insert buffer file_rec/async<cr>
    nnoremap <C-p> :<C-u>call <SID>uniteCtrlP()<CR>
    if executable("ag")
        let g:unite_source_rec_async_command= 'ag --follow --nocolor --nogroup --hidden -g ""'
    endif
    if has('nvim') && 0
        let s:file_rec_cmd = "file_rec/neovim:!"
    else
        let s:file_rec_cmd = "file_rec/async:!"
    endif
    fun! s:uniteCtrlP()
        " code
        let l:commands = "buffer "
        let l:host = expand("%:p:h")
        if l:host =~ 'ssh://'
            let l:commands = l:commands . l:host
        else
            let l:commands = l:commands . s:file_rec_cmd
        endif
        exec "UniteWithProjectDir -buffer-name=files -start-insert " . l:commands
    endf
    "nnoremap <C-t> :<C-u>Unite  -buffer-name=projects     -start-insert prosession/projects<cr>
    nnoremap <leader>m :<C-u>Unite  -buffer-name=mru     -start-insert file_mru<cr>
    "nnoremap <leader>o :<C-u>Unite -no-split -buffer-name=outline -start-insert outline<cr>
    nnoremap <leader>y :<C-u>Unite  -buffer-name=yank    history/yank<cr>
    "nnoremap <leader><Tab> :<C-u>Unite  -buffer-name=buffer  -quick-match buffer<cr>

    " Custom mappings for the unite buffer
    autocmd FileType unite call <SID>unite_settings()
    function! s:unite_settings()
        " Play nice with supertab
        let b:SuperTabDisabled=1
        " Enable navigation with control-j and control-k in insert mode
        map <buffer> <ESC>   <Plug>(unite_exit)
        imap <buffer> <ESC>   <Plug>(unite_exit)
        imap <buffer> <C-j>   <Plug>(unite_select_next_line)
        imap <buffer> <C-k>   <Plug>(unite_select_previous_line)
    endfunction

    "fun! s:AutoloadSession()
        "if @% == ""
            "UniteSessionLoad
        "endif
    "endfun

    "augroup Unite
        "au!
        "autocmd VimEnter * call <SID>AutoloadSession()
    "augroup END
endif
"}}}

" SideBar "{{{
" -------
    fun! s:SetupPlugins()
    if exists(":SideBarAddCmd")
        nmap <silent> <Tab> :SideBarEnterToggle<CR>
        map <silent> <Esc><Tab> :SideBarCycle<CR>
    "  SideBarAddCmd VTreeExplore
    "  Calendar
    "  let l:newwidth=winwidth('.')
    "  SideBarSwallow
    "  close
    "  let g:SideBar_max_width = l:newwidth
        SideBarAddCmd TagListJr
        SideBarAddCmd BufExplorerJr
        SideBarAddCmd ProjectJr ~/.vim/global.prj
    endif
    endfun
    augroup PluginInit
    au!
    au VimEnter * call <SID>SetupPlugins()
    if exists("*SideBarAddCmd")
        au BufAdd * if expand('<afile>') == expand('%') | 
            \ SideBarAddCmd BufExplorerJr | endif
        au BufAdd * SideBarAddCmd BufExplorerJr
    endif
    augroup END

"}}}
"
"" Vim-Markdown {{{
"let g:vim_markdown_initial_foldlevel=1
let g:vim_markdown_folding_disabled=1
let g:vim_markdown_frontmatter=1
""}}}
"" Vim-Pandoc {{{
let g:pandoc#after#modules#enabled = ["ultisnips"]
let g:pandoc#syntax#codeblocks#embeds#langs=['python','cpp','html','go']
let g:pandoc#folding#fdc=0
let g:pandoc#formatting#mode='ha'
""}}}
"
" Clang-complete {{{
"
let g:clang_cpp_options = '-std=c++11'
" }}}
"
" tagbar {{{
"
let g:tagbar_width = 15
let g:tagbar_left = 1
augroup tagBar
au!
"autocmd VimEnter * nested :call tagbar#autoopen(1)
"autocmd FileType * nested :call tagbar#autoopen(0)
"autocmd BufEnter * nested :call tagbar#autoopen(0)
augroup end
" }}}
" UltiSnips and YCM integration {{{
"if exists(":YcmCompleter")
if 1
    let g:ycm_extra_conf_globlist = ['~/dev/*','!~/*']
    let g:ycm_autoclose_preview_window_after_completion = 1
    function! g:UltiSnips_Complete()
        call UltiSnips#ExpandSnippet()
        if g:ulti_expand_res == 0
            if pumvisible()
                return "\<C-n>"
            else
                call UltiSnips#JumpForwards()
                if g:ulti_jump_forwards_res == 0
                return "\<TAB>"
                endif
            endif
        endif
        return ""
    endfunction

    au BufEnter * exec "inoremap <silent> " . g:UltiSnipsExpandTrigger . " <C-R>=g:UltiSnips_Complete()<cr>"
    let g:UltiSnipsExpandTrigger="<C-L>"
    let g:UltiSnipsJumpForwardTrigger="<tab>"
    let g:UltiSnipsListSnippets="<c-h>"

    function! g:JInYCM()
        if pumvisible()
            return "\<C-n>"
        else
            return "\<c-j>"
        endif
    endfunction

    function! g:KInYCM()
        if pumvisible()
            return "\<C-p>"
        else
            return "\<c-k>"
        endif
    endfunction

    fun! JumpToDef()
      if exists("*GotoDefinition_" . &filetype)
        call GotoDefinition_{&filetype}()
      else
        exe "norm! \<C-]>"
      endif
    endf
    
    " Jump to tag
    nn <M-g> :call JumpToDef()<cr>
    ino <M-g> <esc>:call JumpToDef()<cr>i

    "inoremap <c-j> <c-r>=g:JInYCM()<cr>
    inoremap <expr><c-j>  pumvisible() ? "\<C-n>" : "\<C-j>"
    au BufEnter,BufRead * exec "inoremap <silent> " . g:UltiSnipsJumpBackwardTrigger . " <C-R>=g:KInYCM()<cr>"
    au FileType python :nnoremap <buffer> <CR> :YcmCompleter GoTo<CR>
    au FileType nim :nnoremap <buffer> <CR> :<C-u>call JumpToDef()<CR>
    let g:UltiSnipsJumpBackwardTrigger = "<c-k>"
    nnoremap <silent> <Leader><CR> :YcmCompleter GoTo<CR>
endif
"if exists(":NeoCompleteEnable")
if 0
    "let g:jedi#popup_on_dot = 0
    "let g:jedi#auto_initialization = 1
    "let g:jedi#auto_vim_configuration = 1
    "Note: This option must set it in .vimrc(_vimrc).  NOT IN .gvimrc(_gvimrc)!
    " Disable AutoComplPop.
    let g:acp_enableAtStartup = 0
    " Use neocomplete.
    let g:neocomplete#enable_at_startup = 1
    " Use smartcase.
    let g:neocomplete#enable_smart_case = 1
    " Set minimum syntax keyword length.
    let g:neocomplete#sources#syntax#min_keyword_length = 3
    let g:neocomplete#lock_buffer_name_pattern = '\*ku\*'

    " Define dictionary.
    let g:neocomplete#sources#dictionary#dictionaries = {
        \ 'default' : '',
        \ 'vimshell' : $HOME.'/.vimshell_hist',
        \ 'scheme' : $HOME.'/.gosh_completions'
    \ }

    " Define keyword.
    if !exists('g:neocomplete#keyword_patterns')
        let g:neocomplete#keyword_patterns = {}
    endif
    let g:neocomplete#keyword_patterns['default'] = '\h\w*'

    " Plugin key-mappings.
    "inoremap <expr><C-g>     neocomplete#undo_completion()
    inoremap <expr><C-l>     neocomplete#complete_common_string()


    " Recommended key-mappings.
    " <CR>: close popup and save indent.
    inoremap <silent> <CR> <C-r>=<SID>my_cr_function()<CR>
    function! s:my_cr_function()
        "return neocomplete#close_popup() . "\<CR>"
        " For no inserting <CR> key.
        return pumvisible() ? neocomplete#close_popup() : "\<CR>"
    endfunction
    " <TAB>: completion.
    inoremap <expr><TAB>  pumvisible() ? "\<C-n>" : "\<TAB>"
    " <C-h>, <BS>: close popup and delete backword char.
    inoremap <expr><C-h> neocomplete#smart_close_popup()."\<C-h>"
    inoremap <expr><BS> neocomplete#smart_close_popup()."\<C-h>"
    inoremap <expr><C-y>  neocomplete#close_popup()
    inoremap <expr><C-e>  neocomplete#cancel_popup()
    " Close popup by <Space>.
    "inoremap <expr><Space> pumvisible() ? neocomplete#close_popup() : "\<Space>"

    " For cursor moving in insert mode(Not recommended)
    "inoremap <expr><Left>  neocomplete#close_popup() . "\<Left>"
    "inoremap <expr><Right> neocomplete#close_popup() . "\<Right>"
    "inoremap <expr><Up>    neocomplete#close_popup() . "\<Up>"
    "inoremap <expr><Down>  neocomplete#close_popup() . "\<Down>"
    " Or set this.
    "let g:neocomplete#enable_cursor_hold_i = 1
    " Or set this.
    "let g:neocomplete#enable_insert_char_pre = 1

    " AutoComplPop like behavior.
    "let g:neocomplete#enable_auto_select = 1

    " Shell like behavior(not recommended).
    "set completeopt+=longest
    "let g:neocomplete#enable_auto_select = 1
    "let g:neocomplete#disable_auto_complete = 1
    "inoremap <expr><TAB>  pumvisible() ? "\<Down>" : "\<C-x>\<C-u>"

    " Enable omni completion.
    augroup NeoComplete
        au!
        autocmd FileType css setlocal omnifunc=csscomplete#CompleteCSS
        autocmd FileType html,markdown setlocal omnifunc=htmlcomplete#CompleteTags
        autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS
        "autocmd FileType python setlocal omnifunc=pythoncomplete#Complete
        autocmd FileType python setlocal omnifunc=jedi#completions
        autocmd FileType xml setlocal omnifunc=xmlcomplete#CompleteTags
    augroup END

    " Enable heavy omni completion.
    if !exists('g:neocomplete#sources#omni#input_patterns')
        let g:neocomplete#sources#omni#input_patterns = {}
    endif
    "let g:neocomplete#sources#omni#input_patterns.php = '[^. \t]->\h\w*\|\h\w*::'
    "let g:neocomplete#sources#omni#input_patterns.c = '[^.[:digit:] *\t]\%(\.\|->\)'
    "let g:neocomplete#sources#omni#input_patterns.cpp = '[^.[:digit:] *\t]\%(\.\|->\)\|\h\w*::'

    " For perlomni.vim setting.
    " https://github.com/c9s/perlomni.vim
    let g:neocomplete#sources#omni#input_patterns.perl = '\h\w*->\h\w*\|\h\w*::'
    let g:UltiSnipsExpandTrigger="<C-L>"
endif
""}}}

" GitGutter {{{ "
    hi clear SignColumn
    hi link SignColumn LineNr
    highlight link GitGutterAdd DiffAdd
    highlight link GitGutterChange DiffChange
    highlight link GitGutterDelete DiffDelete
    "let g:gitgutter_sign_column_always = 1
" }}} GitGutter "
"
" Netrw {{{ "
let g:netrw_liststyle=3
let g:netrw_winsize=20
"nnoremap <silent> <BS> :Lexplore<CR>
"nnoremap <silent>  :Lexplore<CR>
" }}} Netrw "
"
" Projects for Unite {{{ "
" }}} Projects for Unite "
    let g:unite_project_folder = '~/dev'
    let g:unite_project_list_command = 'find -L %s -type d -name .git -maxdepth 3 | sed -e "s#/.git##g"'
"}}}
"
" XOLOX Sessions {{{ "
"let g:session_autosave = "yes"
let g:session_autoload = "yes"
let g:session_default_to_last = "yes"
" }}} XOLOX Sessions "
"
" {{{ OCaml
"
set rtp+=/usr/local/share/ocamlmerlin/vim
" }}}
"
" Neomake {{{
"
" Build in current buffer
nmap <leader>b :<C-u>silent wa<CR>:Neomake!<CR>

let g:neomake_open_list=1
let g:neomake_place_signs=1
let g:neomake_list_height=7

let g:neomake_nim_check_maker = {
    \ 'exe': 'nim',
    \ 'args': ['check'],
    \ }
let g:neomake_nim_enabled_makers = ['check']

let g:neomake_go_lint_maker = {
    \ 'exe': 'gometalinter',
    \ 'args': ['-t'],
    \ 'errorformat':
        \ '%f:%l:%c:%tarning: %m,' .
        \ '%f:%l:%c:%trror: %m,' .
        \ '%-G#%.%#'
    \ }

let g:neomake_go_test_maker = {
    \ 'exe': 'go',
    \ 'args': ['test', '-v'],
    \ 'errorformat':
        \ '%E%f:%l: %m,' .
        \ '%-G#%.%#'
    \ }

"let g:neomake_go_enabled_makers = ['lint', 'test']
" }}}
"
" Nim "{{{
" -------
    augroup Nim
    au!
    au BufWritePost *.nim :Neomake
    augroup end
"}}}
"
" Templates{{{
let g:skeleton_replacements = {}

function! g:skeleton_replacements.CLASS()
    let l:filen = tolower(expand("%:t:r"))
    return substitute(l:filen, "\\([a-zA-Z]\\+\\)", "\\u\\1\\e", "g")
endfunction
function! g:skeleton_replacements.NAME()
    return tolower(expand("%:t:r"))
endfunction
" "}}}
