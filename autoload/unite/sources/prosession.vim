let s:save_cpo = &cpo
set cpo&vim

let s:unite_source = {
      \ 'name': 'prosession',
      \ 'hooks': {},
      \ 'default_action': {'cdable': 'rec/async'},
      \ }

let g:unite_prosession_list_command =
    \ substitute(get(g:, 'unite_prosession_list_command', 'find @dir -type f -name "*.vim" -printf "%f\n"  -maxdepth 3 | sed -e "s@%@/@g" -e "s@.vim@@"'), "@dir", g:prosession_dir, "")

let g:unite_project_folder =
    \ get(g:, 'unite_project_folder', '~')

let g:unite_prosession_project_list_command =
    \ substitute(get(g:, 'unite_prosession_project_list_command', 'find -L @dir -type d -name .git -maxdepth 3 | sed -e "s#/.git##g"'), "@dir", g:unite_project_folder, "")

function! s:unite_source.gather_candidates(args, context)
    let dirlist = split(system(g:unite_prosession_list_command),'\n')

    return map(dirlist, '{
            \ "word": v:val,
            \ "source": "prosession",
            \ "kind": "command",
            \ "action__command": "Prosession " . v:val,
            \ }')
endfunction

function! unite#sources#prosession#activate_project(dir)
    exec "cd " . a:dir
    exec "Prosession " . a:dir
    if @% == ""
        Unite  -buffer-name=files   -start-insert file_rec/async
    endif
endfunction

let s:unite_source_projects = deepcopy(s:unite_source)
let s:unite_source_projects.name = 'prosession/projects'
function! s:unite_source_projects.gather_candidates(args, context)
    let dirlist = split(system(g:unite_prosession_project_list_command),'\n')

    return map(dirlist, '{
            \ "word": v:val,
            \ "source": "prosession/projects",
            \ "kind": "command",
            \ "action__command": "call unite#sources#prosession#activate_project(\"".v:val."\")",
            \ }')
endfunction

function! unite#sources#prosession#define()
    return [s:unite_source, s:unite_source_projects]
endfunction


let &cpo = s:save_cpo
unlet s:save_cpo
