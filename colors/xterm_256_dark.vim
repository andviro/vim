set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "xterm_256_dark"
set term=xterm-256color

"hi Normal ctermbg=22 cterm=none ctermfg=230
"hi Normal ctermbg=22 cterm=none ctermfg=230
hi StatusLine ctermbg=248 cterm=none ctermfg=0
hi StatusLineNC ctermbg=245 cterm=none ctermfg=0
hi User1 cterm=none ctermfg=160 ctermbg=248
hi User2 cterm=bold ctermfg=62 ctermbg=248
hi User3 cterm=none ctermfg=24 ctermbg=248

hi PreProc ctermfg=219 cterm=bold
hi Statement ctermfg=223
hi Type ctermfg=155
hi Special ctermfg=191
hi Constant ctermfg=210
hi String ctermfg=153
hi Comment ctermfg=152 cterm=bold
hi SpecialChar ctermfg=105
hi Delimiter ctermfg=220
hi Number ctermfg=205 cterm=bold
hi Preprocessor cterm=none ctermfg=183
hi nontext cterm=none ctermfg=93
hi Error cterm=bold,underline ctermfg=9 ctermbg=0
hi ErrorMsg ctermbg=160 ctermfg=7

hi Todo ctermbg=220 ctermfg=0
hi ModeMsg ctermfg=231
hi Visual cterm=none ctermfg=0 ctermbg=188
hi VisualNOS cterm=none ctermfg=0 ctermbg=183

