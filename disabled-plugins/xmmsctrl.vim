" Plugin: xmmsctrl.vim
" Version: 0.3
" Purpose: simple XMMS(2) control through 'smart' buffer
" Author: Andrew Rodionoff (arnost AT mail DOT ru)
"
" New in version 0.3:
" - Total rewrite
" - XMMS2 support
" - Integrated songchange client
" - Works with SideBar 
" - Works with netrw-browser and ProjectJr
" - songlist can be edited and rearranged
"
" Requires: 
" Vim 7.0+, python 2.x and xmms2-client python library for playlist sync
"
" Usage: unpack files into $VIM folder
" use ':XMMS2' to open songlist window
"
" use ':XMMS2 add <file>' to add files
"
" when in netrw browser or ProjectJr window
" :XMMS2
" to load file at cursor
" visual select and
" :'<,'>XMMS2
" to load selected files
"
" songs in songlist window can be rearranged or deleted
" :w songlist to upload it to XMMS2
"
" use :XMMS2 for all xmms2 client commands, man xmms2 for details
"
" Notes:
" - Use command similar to:
"   vim --remote-expr 'RunXMMS2('list')'
"   to popup songlist from shell or window manager menu
"
" Variables:
"   g:XMMS_TagEncoding -- set it to some valid encoding name if needed
"   g:XMMS2_StartInSidebar -- set to 0 to disable sidebar insertion
"
" Bugs:
" - ???
"
" TODO:
" - Decent :XMMS2 arguments completion

fun! s:SID()
  return matchstr(expand('<sfile>'), '<SNR>\zs\d\+\ze_SID$')
endfun

fun! s:XMMS2Cmd(cmd)
  return substitute(system("xmms2 " . a:cmd), '\n$', '', '')
endfun

fun! s:ExecInXMMSWindow(expr)
  let l:xmmsWin = bufwinnr("#XMMS2#") 
  if l:xmmsWin == -1
      return
  endif
  let l:ei = &eventignore
  set eventignore=all
  let l:go_back = 0
  if l:xmmsWin != winnr()
      let l:go_back = 1
      exec l:xmmsWin . 'wincmd w'
  endif
  exec a:expr
  if l:go_back
      wincmd p
  endif
  let &eventignore = l:ei
  redraw!
endfun

fun! s:ExitCallback()
  redraw
  echomsg "xmms2client-vim quit"
  if exists('g:XMMS2ClientPID')
    unlet g:XMMS2ClientPID
  endif
endfu

fun! s:PosCallback(pos)
  let g:XMMS2Pos = a:pos + 1
  call s:ExecInXMMSWindow('call s:UpdateSelectorPos()')
endfu

fun! s:PlaylistCallback()
  call s:ExecInXMMSWindow('call s:SetupSelector()')
endfu

fun! s:StatusCallback(state)
  let g:XMMS2Status = state
  redraw!
endfu

fun! s:SetPID(pid)
  let g:XMMS2ClientPID = a:pid
  redraw
  echomsg "xmms2client started."
endfu

fun! s:Initialize()
  if !empty(v:servername)
    for l:pth in split(&rtp, ',')
      if executable(l:pth . '/bin/xmms2client-vim')
        let s:XMMS2Client = l:pth . '/bin/xmms2client-vim'
        break
      endif
    endfor
  endif
  if exists("s:XMMS2Client")
    echomsg "XMMS2: Client sync enabled"
  else
    echomsg "XMMS2: Client sync disabled"
  endif
  call s:RunClient()
endfun

fun! s:RunClient()
  if !exists('g:XMMS2ClientPID') && exists('s:XMMS2Client')
    call system(s:XMMS2Client . ' ' . v:progname . ' ' .
          \ v:servername . ' ' . s:SID() . ' >>~/xmms2client.log 2>&1 &')
  endif
endfun

fun! s:UpdateSelectorPos()
  silent! syn clear XMMSCurrentPos
  exec "syn match XMMSCurrentPos '^\\[" . g:XMMS2Pos ."/[0-9]\\+\\].*'"
  hi link XMMSCurrentPos Search
  silent! exec "0/^\\[" . g:XMMS2Pos . "\\/"
  normal zz
endfun

fun! s:SetupSelector()
  silent %d _
  syn clear
  syn match Comment '^#.*'
  0insert
# Press <Enter> to play song at cursor
.
  if exists('g:XMMS2TagEncoding')
      let l:tenc = &fileencodings
      exec 'set fileencodings=' . g:XMMS2TagEncoding
  endif

  let l:lst = s:XMMS2Cmd("list")
  silent put =l:lst
  silent! %s/^\(->\)\?\s*//g
  if exists('g:XMMS2TagEncoding')
      exec 'set fileencodings=' . l:tenc
  endif
  call s:UpdateSelectorPos()
  setlocal nomodified modifiable noro nowrap noswapfile
  map <silent> <buffer> <Return> :call <SID>JumpToSongAtLine(line("."))<CR>
endfun

fun! s:JumpToSongAtLine(l)
  let l:pat = '^\[\([0-9]\+\)/'
  let l:s = matchstr(getline(a:l),l:pat)
  if l:s != ""
    let l:nr = substitute(l:s, l:pat, '\1', '')
    exec "XMMS2 jump " . l:nr
    XMMS2 play
  endif
endfun

fun! s:ForceUpdate()
  let g:XMMS2Pos = str2nr(s:XMMS2Cmd("current -f '${position}'")) + 1
endfun

fun! s:OpenWindow(mode)
  call s:RunClient()
  let l:xmmsWin = bufwinnr("#XMMS2#") 
  if l:xmmsWin != -1
      exec l:xmmsWin . 'wincmd w'
      return
  endif
  if bufnr('#XMMS2#') != -1
    buffer \#XMMS2\#
    return
  endif
  if a:mode
    edit \#XMMS2\#
  else
    split \#XMMS2\#
  endif
  if !exists('g:XMMS2Pos')
    call s:ForceUpdate()
  endif
  call s:SetupSelector()
  augroup XMMS2
    au!
    au BufWriteCmd \#XMMS2\#  call <SID>ExecInXMMSWindow("call s:UploadPlaylist()")
    au BufWinEnter \#XMMS2\#  call <SID>SetupSelector()
  augroup END
endfun

fun! s:UploadPlaylist()
  call s:XMMS2Cmd("clear")
  let l:pat = '^\[\([0-9]\+\)/\([0-9]\+\)\]'
  let l:idx = 1
  let l:returnIdx = 0
  for l:lnr in range(1,line("$"))
    let l:ln = matchstr(getline(l:lnr), l:pat)
    if l:ln != ""
      let l:pos = substitute(l:ln, l:pat, '\1', '')
      let l:id = substitute(l:ln, l:pat, '\2', '')
      call s:XMMS2Cmd("add 'id:" .  l:id . "'")
      if l:pos == g:XMMS2Pos
        let l:returnIdx = l:idx
      else
        let l:idx = l:idx + 1
      endif
    endif
  endfor
  if l:returnIdx
    call s:XMMS2Cmd("jump " .  l:returnIdx)
  endif
endfun

augroup XMMS2
  au!
  au VimEnter * call <SID>Initialize()
augroup END

fun! s:CmdCompletion(A,L,P)
  return "sidebar\nadd\nclear\nnext\npause\nplay\nprev\nquit\nremove\nshuffle\nstop\nvol"
endfun

fun! s:LoadFileOrPlaylist(dir, fname)
  exec 'cd ' . fnameescape(a:dir)
  if matchstr(a:fname, '\(cue\|m3u\|pls\)$') != ""
    let l:switch = "-P"
  else
    let l:switch = "-f"
  endif
  call s:XMMS2Cmd("add " . l:switch . ' ' . shellescape(a:fname))
  cd -
endfun

fun! RunXMMS2(...)
  if a:0
    if a:1 == "vol"
      if a:0 == 2
        return s:XMMS2Cmd("vol " . a:2)
      else
        return s:XMMS2Cmd("vol " . input("vol [+|-]percent: "))
      endif
    elseif a:1 == "sidebar"
      return s:OpenWindow(1)
    elseif a:1 == "list"
      return s:OpenWindow(0)
    else
      return s:XMMS2Cmd(join(a:000))
    endif
  else
    if exists('b:netrw_curdir')
      return s:LoadFileOrPlaylist(b:netrw_curdir, getline("."))
    elseif exists('b:IsProjectJr_buffer')
      let l:path = ProjectJr_PathAtLine(line("."))
      let l:filename = matchstr(l:path, '[^/]\+$')
      if l:filename != ""
        let l:dir = strpart(l:path, 0, strlen(l:path)-strlen(l:filename))
        return s:LoadFileOrPlaylist(l:dir, l:filename)
    endif
  endif
  return s:OpenWindow(0)
endfun

command! -nargs=* -range -complete=custom,<SID>CmdCompletion XMMS2 :<line1>,<line2>call RunXMMS2(<f-args>)
" vim: ts=2: sw=2: et: fdm=indent
