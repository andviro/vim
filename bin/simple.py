#!/usr/bin/env python
import xmmsclient
import os
import sys, time

def disconnect_cb(player):
    print "bye!"

xmms = xmmsclient.XMMS("test")
try:
	xmms.connect(os.getenv("XMMS_PATH"), disconnect_cb)
except IOError, detail:
	print "Connection failed:", detail
	sys.exit(1)

def my_current_id(result):
    global xmms
    curInfo = xmms.medialib_get_info(result.value())
    curInfo.wait()
    v = curInfo.value()
    print v['artist'], '//', v['album'], '--', v['title']
#    for k in v:
#        print k, v[k]

xmms.broadcast_playback_current_id(my_current_id)
xmms.loop()
