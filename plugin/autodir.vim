augroup AUTOMKDIR
au!
au BufNewFile * call <SID>Check(expand('<afile>'))
augroup END

fun! s:Check(fname)
    let l:fullpath = fnamemodify(a:fname, ':p:~:.:h')
    if ! isdirectory(l:fullpath)
        call system('mkdir -p ' . l:fullpath)
    endif
endfun
