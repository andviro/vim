# coding: utf-8

import os
import sys

if sys.platform == 'win32':
    lib_dir = 'Lib'
else:
    lib_dir = os.path.join('lib', 'python{0}'.format(sys.version[:3]))


def setup_path(env):
    target = os.path.join(env, lib_dir, 'site-packages')
    if os.path.isdir(target):
        if target in sys.path:
            sys.path[:] = [x for x in sys.path if x != target]
        sys.path.insert(0, target)
        sys.path.insert(0, os.path.join(env, lib_dir))
        return True
    return False


def find_env(dirname):
    curdir = os.path.abspath(dirname)
    while not (os.path.isdir(os.path.join(curdir, '.env'))):
        newdir = os.path.normpath(os.path.join(curdir, os.pardir))
        if newdir == curdir:
            return None
        curdir = newdir
    return os.path.join(curdir, '.env')

if __name__ == '__main__':
    print(find_env('.'))
