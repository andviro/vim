
set background=light

hi clear
if exists("syntax_on")
  syntax reset
endif

let colors_name = "pyte256"

if v:version >= 700
  hi CursorLine guibg=#f6f6f6 ctermbg=15
  hi CursorColumn guibg=#eaeaea ctermbg=7
  hi MatchParen guifg=white guibg=#80a090 ctermfg=white ctermbg=108 cterm=bold gui=bold

  "Tabpages
  hi TabLine guifg=black guibg=#b0b8c0 ctermbg=250 ctermfg=black cterm=italic gui=italic
  hi TabLineFill guifg=#9098a0 ctermfg=246
  hi TabLineSel guifg=black guibg=#f0f0f0 ctermbg=7 ctermfg=black cterm=italic,bold gui=italic,bold

  "P-Menu (auto-completion)
  hi Pmenu guifg=white guibg=#808080 ctermbg=244 ctermfg=white
  "PmenuSel
  "PmenuSbar
  "PmenuThumb
endif
"
" Html-Titles
hi Title      guifg=#202020 ctermfg=234 cterm=bold gui=bold
hi Underlined  guifg=#202020 ctermfg=234 cterm=underline gui=underline


hi Cursor    guifg=black   guibg=#b0b4b8 ctermbg=249 ctermfg=black
hi lCursor   guifg=black   guibg=white ctermbg=15 ctermfg=black ctermbg=white
hi LineNr    guifg=#ffffff ctermfg=15 guibg=#c0d0e0 ctermbg=152

hi Normal    guifg=#404850 ctermfg=238   guibg=#f0f0f0 ctermbg=7
hi Visual    ctermbg=251

hi StatusLine guifg=white guibg=#8090a0 ctermbg=103 ctermfg=white cterm=bold,italic gui=bold,italic
hi StatusLineNC guifg=#506070 ctermfg=59 guibg=#a0b0c0 ctermbg=145 cterm=italic gui=italic
hi VertSplit guifg=#a0b0c0 ctermfg=145 guibg=#a0b0c0 ctermbg=145 cterm=NONE gui=NONE

" hi Folded    guifg=#708090 ctermfg=66 guibg=#c0d0e0 ctermbg=152
hi Folded    guifg=#a0a0a0 ctermfg=247 guibg=#e8e8e8 ctermbg=7 cterm=italic gui=italic

hi NonText   guifg=#c0c0c0 ctermfg=250 guibg=#e0e0e0 ctermbg=7
" Kommentare
hi Comment   guifg=#a0b0c0 ctermfg=145 cterm=italic cterm=italic gui=italic

" Konstanten
hi Constant  guifg=#a07040 ctermfg=131
hi String    guifg=#4070a0 ctermfg=61 
hi Number    guifg=#40a070 ctermfg=71
hi Float     guifg=#70a040 ctermfg=71
"hi Statement guifg=#0070e0 ctermfg=26 cterm=NONE gui=NONE
" Python: def and so on, html: tag-names
hi Statement  guifg=#007020 ctermfg=22 cterm=bold gui=bold


" HTML: arguments
hi Type       guifg=#e5a00d ctermfg=178 cterm=italic gui=italic
" Python: Standard exceptions, True&False
hi Structure  guifg=#007020 ctermfg=22 cterm=italic gui=italic
hi Function   guifg=#06287e ctermfg=18 cterm=italic gui=italic

hi Identifier guifg=#5b3674 ctermfg=240 cterm=italic gui=italic

hi Repeat      guifg=#7fbf58 ctermfg=107 cterm=bold gui=bold
hi Conditional guifg=#4c8f2f ctermfg=64 cterm=bold gui=bold

" Cheetah: #-Symbol, function-names
hi PreProc    guifg=#1060a0 ctermfg=25 cterm=NONE gui=NONE
" Cheetah: def, for and so on, Python: Decorators
hi Define      guifg=#1060a0 ctermfg=25 cterm=bold gui=bold

hi Error      guifg=red guibg=white ctermfg=red ctermbg=white cterm=bold,underline gui=bold,underline
hi Todo       guifg=#a0b0c0 ctermfg=145 guibg=NONE cterm=italic,bold,underline gui=italic,bold,underline

" Python: %(...)s - constructs, encoding
hi Special    guifg=#70a0d0 ctermfg=74 cterm=italic gui=italic

hi Operator   guifg=#408010 ctermfg=64

" color of <TAB>s etc...  
"hi SpecialKey guifg=#d8a080 ctermfg=180 guibg=#e8e8e8 ctermbg=7 cterm=italic gui=italic  
hi SpecialKey guifg=#d0b0b0 ctermfg=181 guibg=#f0f0f0 ctermbg=7 cterm=none gui=none

" Diff
hi DiffChange guifg=NONE guibg=#e0e0e0 ctermbg=7 ctermfg=NONE cterm=italic,bold gui=italic,bold
hi DiffText guifg=NONE guibg=#f0c8c8 ctermbg=224 ctermfg=NONE cterm=italic,bold gui=italic,bold
hi DiffAdd guifg=NONE guibg=#c0e0d0 ctermbg=152 ctermfg=NONE cterm=italic,bold gui=italic,bold
hi DiffDelete guifg=NONE guibg=#f0e0b0 ctermbg=223 ctermfg=NONE cterm=italic,bold gui=italic,bold


