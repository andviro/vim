" local syntax file - set colors on a per-machine basis:
" vim: tw=0 ts=4 sw=4
" Vim color file
" Maintainer:	Steven Vertigan <steven@vertigan.wattle.id.au>
" Last Change:	2001 Sep 10
" Revision #4: Support for new "Underline" group. Removed superfluous html "              formatting.

set background=dark
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "extra_blue"
hi Normal		guifg=#c0e0e0		guibg=#000040
hi Normal		ctermfg=white	ctermbg=darkBlue
hi NonText		guifg=magenta	ctermfg=lightMagenta
hi comment		guifg=lightslateblue		gui=italic
hi comment		ctermfg=gray
hi constant		guifg=cyan		ctermfg=cyan
hi identifier	guifg=gray		ctermfg=gray
hi statement	guifg=yellow	gui=none	ctermfg=yellow
hi preproc		guifg=green		ctermfg=green
hi type			guifg=orange	ctermfg=darkYellow
hi special		guifg=magenta	ctermfg=lightMagenta
hi Underlined	guifg=cyan 		ctermfg=cyan
hi Underlined	gui=underline	cterm=underline

hi ErrorMsg		guifg=orange	guibg=darkBlue	
hi ErrorMsg		ctermfg=lightRed
hi WarningMsg	guifg=cyan		guibg=darkBlue	gui=italic	
hi WarningMsg	ctermfg=cyan
hi ModeMsg		guifg=yellow	gui=NONE
hi ModeMsg		ctermfg=yellow
hi MoreMsg		guifg=yellow	gui=NONE
hi MoreMsg		ctermfg=yellow
hi Error		guifg=red		guibg=darkBlue	gui=underline
hi Error		ctermfg=red

hi Todo			guifg=black		guibg=orange
hi Todo			ctermfg=black	ctermbg=darkYellow
hi Cursor		guifg=black		guibg=green
hi Cursor		ctermfg=black	ctermbg=green
hi lCursor		guifg=black		guibg=red
hi lCursor		ctermfg=black	ctermbg=red
hi Search		guifg=black		guibg=orange
hi Search		ctermfg=black	ctermbg=darkYellow
hi IncSearch	guifg=black		guibg=yellow
hi IncSearch	ctermfg=black	ctermbg=darkYellow
hi LineNr		guifg=pink		ctermfg=lightMagenta
hi title		guifg=white	gui=italic	cterm=bold

hi StatusLine gui=NONE guifg=black guibg=lightsteelblue
hi StatusLineNC gui=NONE guifg=gray25 guibg=lightsteelblue
hi User1 guifg=red4 guibg=lightsteelblue gui=bold
hi User2 guifg=darkblue guibg=lightsteelblue gui=bold
hi User3 guifg=darkgreen guibg=lightsteelblue gui=bold

hi label		guifg=yellow	ctermfg=yellow
hi operator		guifg=orange	gui=italic	ctermfg=darkYellow
hi clear Visual
hi Visual		term=reverse 
hi Visual 		ctermfg=black	ctermbg=darkCyan
hi Visual		guifg=black		guibg=lightslategray

hi DiffChange	guibg=darkGreen		guifg=black
hi DiffChange	ctermbg=darkGreen	ctermfg=black
hi DiffText		guibg=olivedrab		guifg=black
hi DiffText		ctermbg=lightGreen	ctermfg=black
hi DiffAdd		guibg=slateblue		guifg=black
hi DiffAdd		ctermbg=blue		ctermfg=black
hi DiffDelete   guibg=coral			guifg=black	
hi DiffDelete	ctermbg=cyan		ctermfg=black

hi Folded		guibg=orange		guifg=black	
hi Folded		ctermbg=yellow		ctermfg=black
hi FoldColumn	guibg=gray30		guifg=black
hi FoldColumn	ctermbg=gray		ctermfg=black
hi cIf0			guifg=gray			ctermfg=gray

