fun! s:Uncode(str, enc)
  return iconv(iconv(a:str, &enc, &fenc), a:enc, &enc)
endfun

fun! s:UncodeGroup(line1, line2, enc)
  let l:tline = a:line1
  while l:tline <= a:line2
    let l:str = getline(l:tline)
    call setline(l:tline, s:Uncode(l:str, a:enc))
    let l:tline = l:tline + 1
  endwhile
endfun


command! -range -nargs=1 Uncode :call <SID>UncodeGroup(<line1>, <line2>, <f-args>)
