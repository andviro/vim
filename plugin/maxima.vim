fun! Maxima_filter(l1,l2)
    exec a:l1. "," . a:l2 . "y z"
    exec a:l1. "," . a:l2 . "!sed 's/^.//' | maxima | 
        \ awk 'BEGIN {c=0} /(C[0-9])/ { c=c + 1 } 
        \ /.*/ { if (c == 1) print \"\\". @z[0] ."\" $0 }' 2>/dev/null | expand"
    exec a:l1 . "put! z"
endfun

command! -range Maxima call Maxima_filter(<line1>, <line2>)

vmap M :Maxima<CR>
