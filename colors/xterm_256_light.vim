set background=light
hi clear
if exists("syntax_on")
  syntax reset
endif
let g:colors_name = "xterm_256_light"

set term=xterm-256color
hi Ignore ctermfg=252 cterm=none
"hi Normal ctermbg=253 cterm=none ctermfg=0
"hi StatusLine ctermbg=254 cterm=none ctermfg=0
hi Folded ctermbg=253 ctermfg=darkblue
"hi StatusLineNC ctermbg=252 cterm=none ctermfg=0
"hi User1 cterm=none ctermfg=160 ctermbg=254
"hi User2 cterm=bold ctermfg=19 ctermbg=254
"hi User3 cterm=none ctermfg=22 ctermbg=254
hi StatusLine ctermbg=248 cterm=none ctermfg=0
"hi Folded ctermbg=249 ctermfg=0
"hi Folded ctermbg=177 ctermfg=0
hi FoldColumn ctermbg=249 ctermfg=darkblue
hi StatusLineNC ctermbg=246 cterm=none ctermfg=0"{{{
hi User1 cterm=none ctermfg=160 ctermbg=248
hi User2 cterm=bold ctermfg=19 ctermbg=248"}}}
hi User3 cterm=none ctermfg=22 ctermbg=248

hi PreProc ctermfg=89 cterm=bold
hi Statement ctermfg=19
hi Type ctermfg=90
hi Special ctermfg=90
hi Constant ctermfg=54
hi String ctermfg=22 cterm=bold
hi Comment ctermfg=30 cterm=none
hi SpecialChar ctermfg=126
hi Delimiter ctermfg=22
hi Number ctermfg=124 cterm=bold
hi Preprocessor cterm=none ctermfg=199
hi nontext cterm=none ctermfg=241
hi Error cterm=bold,underline ctermfg=196 ctermbg=none
hi ErrorMsg ctermbg=160 ctermfg=7
hi Identifier ctermfg=55 cterm=bold

hi Todo ctermbg=190 ctermfg=0
hi ModeMsg ctermfg=18
"hi Visual cterm=none ctermfg=0 ctermbg=110
hi Visual cterm=none ctermfg=0 ctermbg=153
hi VisualNOS cterm=none ctermfg=0 ctermbg=111

hi Question ctermfg=22 cterm=bold
hi MoreMsg ctermfg=22 cterm=bold
